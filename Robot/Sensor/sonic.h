/**
 *      @file   sonic.h
 *
 *      @brief  Header file for sonic.c
 *
 *              Library of functions for reading the Sonic Sensor
 *
 *      @author Quentin Urbanowicz
 *      @author Rene Chavez
 *      @author Peyton Sher
 *      @author Don Kieu
 *
 *      @date   2018-11-13, 2018-11-27
 */

#include <Utils/timer.h>
#include <stdbool.h>
#include <inc/tm4c123gh6pm.h>
#include "driverlib/interrupt.h"

/**
 * Sonic sensor initialization
 */
void sonic_init();

/**
 * Sends Sonic Pulse
 */
void sonic_chirp();

/**
 * Handler to catch Sonic Pulse Return and record timing of Sonic Pulse
 */
void sonic_handler();

/**
 * Initialize the sonic sensor to output chirp
 */
void chirp_init();

/**
 * Uses Chirp init and sonic chirp to send out sonic pulse
 */
void sonic_start();

/**
 * Gets the raw reading of last sonic pulse timing
 *
 *  @return The time between sending and receiving the most recent
 * 			sonic ping in timer ticks (as captured by the input capture timer)
 */
int get_sonic_reading();

/**
 * Converts the given raw sonic sensor value to a distance in centimeters
 *
 *  @param rawSonicValue
 *				- the time between sending and receiving the sonic ping
 *				  in timer ticks (as captured by the input capture timer)
 *
 *  @return the distance to the object detected in centimeters
 */
float sonic_convertToDist(int rawSonicValue);
