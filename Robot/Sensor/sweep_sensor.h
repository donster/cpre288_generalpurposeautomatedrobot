/**
 *      @file   sweep_sensor.h
 *
 *      @brief  Header file for sweep_sensor.c
 *
 *              Library of functions for controlling and reading data from
 *              the sensors in the servo arm assembly.
 *
 *      @author Quentin Urbanowicz
 *      @author Rene Chavez
 *      @author Peyton Sher
 *      @author Don Kieu
 *
 *      @date   2018-11-27
 */

#ifndef SWEEP_SENSOR_H_
#define SWEEP_SENSOR_H_

#include "Sensor/ir.h"
#include "Sensor/sonic.h"
#include "Communication/uart.h"
#include <stdint.h>
#include <stdio.h>
#include <math.h>
#include <inc/tm4c123gh6pm.h>

/**
 * Initializes the sweep sensor assembly and all of its constituent components.
 */
void sweep_init();

/**
 * Returns the current angular position of the sweep sensor assembly.
 *
 * @return The angular position in degrees of the sweep sensor on the range [-90, 90],
 *         where 0 degrees is the center/forward position, -90 is its rightmost (CW) deflection,
 *         and +90 is its leftmost (CCW) deflection.
 */
float sweep_getPos();

/**
 * Rotates the sweep sensor assembly to the given absolute angular position.
 *
 * @param angularPos_deg
 *            - The angular position to move the sensor to in degrees.
 *              This value must be on the interval [-90, 90], 
 *              where -90 is the rightmost position, 0 is the center position,
 *              and +90 is the leftmost position.
 *              If the given value is not on this interval, this function call
 *              will set the servo position to its maximum deflection in the
 *              direction specified by the sign of this value.
 *
 * @return The approximate amount of time, in milliseconds, that it will take
 *         for the sensor assembly to move to its new position after this
 *         function is called.
 */
uint32_t sweep_moveAbsolute(float angularPos_deg);

/**
 * Rotates the sweep sensor assembly from its current angular position by a given amount.
 *
 * @param angularOffset_deg
 *            - The direction and amount to move the sensor from its current position in degrees.
 *              This value must be on the interval [-180, 180],
 *              where a negative value specifies a counterclockwise ("leftward") movement
 *              and a positive value specifies a clockwise ("rightward") movement.
 *              If the given value is not on this interval, this function call
 *              will do nothing.  If the resulting angular position after this function call
 *              is larger than 90 degrees or less than -90 degrees, then the sensor will
 *              be moved to 90 or -90 degrees, respectively (i.e. its maximum deflection).
 *
 * @return The approximate amount of time, in milliseconds, that it will take
 *         for the sensor assembly to move to its new position after this
 *         function is called.
 */
uint32_t sweep_moveRelative(float angularOffset_deg);

/**
 * Returns a reading from the IR sensor connected to the sweep sensor assembly.
 *
 * @return A 12-bit ADC quantization value representing the output of the IR sensor.
 */
int32_t sweep_readIRSensor();

/**
 * Returns a reading from the sonic sensor connected to the sweep sensor assembly.
 *
 * @return Value representing the number of timer ticks between sending and receiving
 *         the most recent sonic sensor ping.
 */
int32_t sweep_readSonicSensor();

/**
 * Performs the sensor sweep routine over a specific range of angles.
 *
 * The sweep sensor assembly sweeps through the specified range, collecting IR and sonic
 * sensor data and transmitting it serially over UART.
 *
 * The sweep is performed starting at startAngle_deg and ending at endAngle_deg (inclusive).
 *
 * The speed and granularity of the sweep is determined by sweepIncrement_deg, which specifies
 * the amount that the sensor assembly's angular distance is incremented by between each sensor
 * reading. The routine terminates when the sensor assembly has swept across the specified
 * range of angles and sent all sensor data over UART.
 *
 * @param startAngle_deg
 *            - The angle at which to begin the sweep, inclusive. This value must lie on the
 *              interval [-90, 90] to be considered valid.
 *
 * @param endAngle_deg
 *            - The angle at which to end the sweep, inclusive. This value must lie on the
 *              interval [-90, 90] to be considered valid.
 *
 * @param sweepIncrement_deg
 *            - The angle, in degrees, by which the sweep sensor assembly will be moved
 *              between each sensor reading. This value determines the granularity of the
 *              sweep performed; a smaller value results in more sensor readings and greater
 *              accuracy, at the expense of greater running time. This value must be greater
 *              than zero in order to be considered valid.
 *
 * @param incDelay_ms
 *            - The amount of additional time, in milliseconds, to wait between taking sensor
 *              readings and incrementing the sweep sensor's angular position.
 */
void sweep_doSweep(float startAngle_deg, float endAngle_deg,
                   float sweepIncrement_deg, uint32_t incDelay_ms);

/**
 * Performs the sensor sweep routine over the full sweep range.
 *
 * The sweep sensor assembly sweeps across its entire range of motion, collecting IR and sonic
 * sensor data and transmitting it serially over UART.

 * The sweep is performed either from left-to-right or from right-to-left, with the sweep
 * starting on whichever end is closest to the sensor assembly's position at the time the
 * function is called. The speed and granularity of the sweep is determined by
 * sweepIncrement_deg, which specifies the amount that the sensor assembly's angular distance
 * is incremented by between each sensor reading. The routine terminates when the sensor
 * assembly has swept across its entire 180-degree range of motion and sent all sensor data
 * over UART.
 *
 * @param sweepIncrement_deg
 *            - The angle, in degrees, by which the sweep sensor assembly will be moved
 *              between each sensor reading. This value determines the granularity of the
 *              sweep performed; a smaller value results in more sensor readings and greater
 *              accuracy, at the expense of greater running time. This value must be greater
 *              than zero in order to be considered valid.
 *
 * @param incDelay_ms
 *            - The amount of additional time, in milliseconds, to wait between taking sensor
 *              readings and incrementing the sweep sensor's angular position.
 */
void sweep_doFullSweep(float sweepIncrement_deg, uint32_t incDelay_ms);

#endif /* SWEEP_SENSOR_H_ */
