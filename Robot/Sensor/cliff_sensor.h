/**
 *      @file   cliff_sensor.h
 *
 *      @brief  Header file for cliff_sensor.c
 *
 *              Library of functions for reading the cliff sensors and
 *              determining the type of boundary being detected.
 *
 *      @author Quentin Urbanowicz
 *      @author Rene Chavez
 *      @author Peyton Sher
 *      @author Don Kieu
 *
 *      @date   2018-11-13, 2018-11-27
 */
 
#ifndef CLIFF_SENSOR_H_
#define CLIFF_SENSOR_H_

#include "Utils/open_interface.h"
#include <stdint.h>
#include <inc/tm4c123gh6pm.h>

/**
 * Enum used to indicate which cliff sensor to read data from
 */
typedef enum
{
    /** Indicates the left cliff sensor */
    LEFT,
    /** Indicates the front-left cliff sensor */
    FRONT_LEFT,
    /** Indicates the front-right cliff sensor */
    FRONT_RIGHT,
    /** Indicates the right cliff sensor */
    RIGHT
} CliffSensor;

/**
 * Enum representing the type of terrain the cliff sensor detects
 */
typedef enum
{
    /** Indicates a gap in the ground/floor */
    CLIFF,
    /** Indicates normal ground */
    GROUND,
    /** Indicates the ground within a work zone */
    ZONE,
    /** Indicates the boundary of the testing field */
    BOUNDARY
} Terrain;

/**
 * Returns the raw strength of the indicated cliff sensor.
 *
 *  @param sensor
 *              - The cliff sensor to be read
 *  @return The strength of the indicated cliff sensor as an
 *          unsigned 16-bit integer
 */
uint16_t cs_readSensorRaw(oi_t *self, CliffSensor sensor);

/**
 * Returns the terrain being detected by the indicated cliff sensor.
 *
 *  @param sensor
 *              - The cliff sensor to be read
 *  @return The type of terrain being sensed as an enum
 */
Terrain cs_getTerrain(oi_t *self, CliffSensor sensor);

/**
 * Checks the ground sensors and returns an int depending on what
 * type of boundary is there.
 *
 *	@param self
 *              - The open interface struct for this bot
 *
 *	@return An int on the interval [0, 3] corresponding to
 *			the type of terrain being detected,
 *			where:
 *				0 corresponds to regular ground,
 *				1 corresponds to a cliff/hole,
 *				2 corresponds to a boundary,
 *			and 3 corresponds to the target work zone
 */
int checkGroundSensors(oi_t* self);

#endif /* CLIFF_SENSOR_H_ */
