/**
 *      @file   ir.h
 *
 *      @brief  Header file for ir.c
 *
 *              Library of functions for reading the IR Sensor
 *
 *      @author Quentin Urbanowicz
 *      @author Rene Chavez
 *      @author Peyton Sher
 *      @author Don Kieu
 *
 *      @date   2018-11-13, 2018-11-27
 */

#include <Utils/timer.h>
#include <stdint.h>
#include <stdbool.h>
#include <inc/tm4c123gh6pm.h>

/**
* Initialize the ADC
*/
void adc_init();

/**
* Read from the ADC buffer of the Micro Controller
*/
int ADC_read();

/**
* Handler that stores the ADC reading of the IR sensor
*/
void adc_handler();

/**
 * Reads and returns the output of the IR sensor as an ADC quantization value
 *
 *  @return The quantization value representing the raw voltage
 *          output of the IR sensor
 */
int getADCReading();

/**
 * Converts the given raw IR sensor value to a distance in centimeters
 *
 * 	@param rawIRValue
 *              - The quantization value representing the raw
 *                voltage output of the IR sensor
 *
 * 	@return The distance to the object detected in centimeters
 */
float ir_convertToDist(int rawIRValue);
