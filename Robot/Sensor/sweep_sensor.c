/**
 *      @file   sweep_sensor.c
 *
 *      @brief  Library of functions for controlling and reading data from
 *              the sensors in the servo arm assembly.
 *
 *      @author Quentin Urbanowicz
 *      @author Rene Chavez
 *      @author Peyton Sher
 *      @author Don Kieu
 *
 *      @date   2018-11-27
 */

#include "sweep_sensor.h"

// -------------------------------- BEGIN CONSTANTS -------------------------------- //

/**
 * The rate at which the PWM timer counts, expressed in timer ticks per second.
 *
 * This value is 16,000,000 because the timer is set to run at the frequency
 * of the system clock, which is 16 MHz
 */
#define TIMER_TICKS_PER_SECOND 16e6

/**
 * The period of the PWM timer in microseconds
 */
#define PWM_PERIOD_MICROS 20000 // 20 milliseconds

/**
 * The rate at which the PWM timer counts, expressed in timer ticks per microsecond.
 */
#define TIMER_TICKS_PER_MICROS (TIMER_TICKS_PER_SECOND * (1.0e-6))

/**
 * The total number of timer ticks in one period of the PWM signal
 */
#define PWM_PERIOD_TICKS (PWM_PERIOD_MICROS * TIMER_TICKS_PER_MICROS) // should be 320,000

// -------------------------------- END CONSTANTS -------------------------------- //

// -------------------------------- BEGIN GLOBAL VARIABLES -------------------------------- //

// GLOBAL VARIABLE(S) FOR PWM:
/**
 * The current match value of the counter being used to generate the PWM signal
 */
unsigned int pos_counter_value;

/**
 * The conversion factor between rotational position of the servo in degrees and
 * the period of the PWM timer in microseconds required to move the servo to that
 * angle.
 *
 * This value can be adjusted via the calibration functions to work with properly
 * with any particular servo motor.
 */
float degrees_per_micros;

/**
 * The constant value used to convert between rotational position of the servo in degrees and
 * the period of the PWM timer in microseconds required to move the servo to that
 * angle.
 *
 * This value can be adjusted via the calibration functions to work with properly
 * with any particular servo motor.
 */
signed int micros_offset;

// GLOBAL VARIABLE(S) FOR GENERAL SERVO CONTROL:
/**
 * The current angular position of the servo in degrees.
 *
 * Valid range: [-90, 90]
 */
volatile float servoPos_deg;

// -------------------------------- END GLOBAL VARIABLES -------------------------------- //

// -------------------------------- BEGIN HELPER FUNCTIONS -------------------------------- //

/**
 * Helper function that calculates the approximate amount of time needed for the servo assembly
 * to move from one position to another position.
 *
 * PRECONDITION: prevPos and newPos are valid angular positions (are on the interval [-90, 90])
 *
 * @param prevPos - the previous angular position in degrees
 * @param newPos - the new angular position in degrees
 * @return Approximate amount of time, in milliseconds, that the servo will take to move
 *         between prevPos and newPos.  
 */
static uint32_t getServoWaitTime_Millis(float prevPos_deg, float newPos_deg)
{
//    return (fabs(prevPos_deg - newPos_deg) / 0.00045); // Servo moves at a rate of approximately 0.00045 degrees per microsecond (180 deg / 400,000 us)
//    return (fabs(prevPos_deg - newPos_deg) / 0.00005);
    return (fabs(prevPos_deg - newPos_deg) * (500 / 90));
}

/**
 * Sets the pulse width of the PWM signal being generated by Timer 1B to the given pulse width
 *
 * @param pulseWidth_micros - pulse width for Timer 1B to generate, in microseconds
 */
static void servo_configurePWM(float pulseWidth_micros)
{
    // Convert pulse width length from microseconds to timer ticks
    // and calculate value to load into Match registers
    //pos_counter_value = (PWM_PERIOD_TICKS - 1) - (pulseWidth_micros * TIMER_TICKS_PER_MICROS); // 16 timer ticks per microsecond
    pos_counter_value = (PWM_PERIOD_TICKS - 1)
            - (pulseWidth_micros * TIMER_TICKS_PER_MICROS); // 16 timer ticks per microsecond

    // Make sure that pulse_width_ticks fits within 24 bits
    pos_counter_value &= 0x00FFFFFF; // Clear upper 8 bits

    // Disable Timer 1B for configuration (clear bit 8 - TBEN)
    TIMER1_CTL_R &= ~0x00000100;

    // Load the 2 high bytes into Timer B Prescale Match register
    TIMER1_TBPMR_R &= 0xFFFFFF00;
    TIMER1_TBPMR_R |= (pos_counter_value >> 16);

    // Load the 4 lower bytes into Timer B Match register
    TIMER1_TBMATCHR_R &= 0xFFFF0000;
    TIMER1_TBMATCHR_R |= (pos_counter_value & 0xFFFF);

    // Re-enable Timer1B and resume PWM-wave generation
    TIMER1_CTL_R |= 0x100;
}

// -------------------------------- END HELPER FUNCTIONS -------------------------------- //

void sweep_init()
{
    /*
     * Part 1:  Configure Timer 1B on pin PB5 for PWM servo control
     */
    // Initialize calibration values to their initial values
    degrees_per_micros = 0.1; // Originally 0.1
    micros_offset = 525;        // Originally 500

    // 1. Configure GPIO

    // Enable clock to GPIO port B
    SYSCTL_RCGCGPIO_R |= 0x02;

    // Set pin 5 of port B as an output
    GPIO_PORTB_DIR_R |= (0x20);

    // Enable Alt functions on PB5
    GPIO_PORTB_AFSEL_R |= 0x20;

    // Select Alt. Function #7 (T1CCP1) for PB5
    GPIO_PORTB_PCTL_R &= ~0x00F00000;
    GPIO_PORTB_PCTL_R |= 0x700000;

    // Enable digital functionality on PB5
    GPIO_PORTB_DEN_R |= 0x20;

    // 2. Configure Timer1B

    // Enable & provide a clock to general-purpose timer module 1 in run mode
    SYSCTL_RCGCTIMER_R |= 0x02;

    // Disable Timer 1B for configuration (clear bit 8 - TBEN)
    TIMER1_CTL_R &= ~0x00000100;

    // Select 16-bit timer configuration for Timer 1B
    TIMER1_CFG_R |= 0x4;

    // Set Timer 1B to PWM mode, Edge-count mode, and Periodic Timer mode
    // (TBAMS = 1, TBCMR = 0, TBMR = 0x2)
    TIMER1_TBMR_R &= ~0x0000000F;
    TIMER1_TBMR_R |= 0xA;

    // Ensure that output state of PWM signal is not inverted
    TIMER1_CTL_R &= ~0x4000;

    // The starting value to set for the timer.
    //
    // It is necessary to subtract 1 from PWM_PERIOD_TICKS
    // because the timer count includes zero.
    unsigned int timer_start_val = PWM_PERIOD_TICKS - 1;

    // Clear bits 31:24 (upper 2 bytes) to ensure that the value will fit
    // into the 24-bit timer (16-bit timer + 8-bit extension)
    timer_start_val &= 0x00FFFFFF;

    // Set the starting count value of the timer to 319,999
    // for a period of 20 milliseconds
    TIMER1_TBPR_R &= 0xFFFF0000;
    //TIMER1_TBPR_R |= (320000 >> 16); // Load high bytes into Timer B Prescale register
    TIMER1_TBPR_R |= (timer_start_val >> 16); // Load high bytes into Timer B Prescale register
    TIMER1_TBILR_R &= 0xFFFF0000;
    //TIMER1_TBILR_R |= (320000 & 0xFFFF); // Load low bytes into Timer B Interval Load register
    TIMER1_TBILR_R |= (timer_start_val & 0xFFFF); // Load low bytes into Timer B Interval Load register

    // The initial timer match value to set in order to generate a pulse width
    // of 1.5 milliseconds (to move the servo motor to its center position)
    pos_counter_value = (PWM_PERIOD_TICKS - 1)
            - (((90 / degrees_per_micros) + micros_offset)
                    * TIMER_TICKS_PER_MICROS); // should be 295,999

    // Clear bits 31:24 (upper 2 bytes) to ensure that the value will fit
    // into the 24-bit timer (16-bit timer + 8-bit extension)
    pos_counter_value &= 0x00FFFFFF;

    // Set the timer match value to an initial value of 295,999
    // to generate a pulse width of 1.5 milliseconds
    TIMER1_TBPMR_R &= 0xFFFFFF00;
    //TIMER1_TBPMR_R |= ((320000 - 24000) >> 16); // Load high bytes into Timer B Prescale Match register
    TIMER1_TBPMR_R |= (pos_counter_value >> 16); // Load high bytes into Timer B Prescale Match register
    TIMER1_TBMATCHR_R &= 0xFFFF0000;
    //TIMER1_TBMATCHR_R |= ((320000 - 24000) & 0xFFFF); // Load low bytes into Timer B Match register
    TIMER1_TBMATCHR_R |= (pos_counter_value & 0xFFFF); // Load low bytes into Timer B Match register

    // Re-enable Timer1B and begin counting
    TIMER1_CTL_R |= 0x100;

    /*
     * Part 2:  Initialize IR and sonic sensors
     */
    // Initialize ADC for IR sensor
    adc_init();

    // Initialize sonic sensor
    sonic_init();

    // Initialize UART for use in sweep routine
    uart_init();
}

float sweep_getPos()
{
    // Return angular position of servo in degrees
    //return ((degrees_per_micros / TIMER_TICKS_PER_MICROS) * (PWM_PERIOD_TICKS - 1) - (degrees_per_micros * (micros_offset + pos_counter_value) ));
    return servoPos_deg;
}

uint32_t sweep_moveAbsolute(float angularPos_deg)
{
    // If the specified angular position is outside the range of valid positions,
    // i.e. outside of [-90,90], then set the servo to its maximum deflection in
    // either the clockwise or counterclockwise direction (as determined by the
    // sign of angularPos_deg)
    if (angularPos_deg > 90)
    {
        angularPos_deg = 90;    // Maximum counterclockwise deflection
    }
    else if (angularPos_deg < -90)
    {
        angularPos_deg = -90;   // Maximum clockwise deflection
    }

    // Calculate pulse width required to move servo motor to this angle
    float pulse_width_micros = ((angularPos_deg + 90.0) / degrees_per_micros)
            + micros_offset;

    // Calculate the time required for servo to move to new position in milliseconds
    uint32_t wait_time_servo = getServoWaitTime_Millis(sweep_getPos(),
                                                       angularPos_deg);

    // Call servo_configurePWM() to move the servo to that position
    servo_configurePWM(pulse_width_micros);

    // Update current position of servo
    servoPos_deg = angularPos_deg;

    // Return wait time for servo
    return wait_time_servo;
}

uint32_t sweep_moveRelative(float angularOffset_deg)
{
    return sweep_moveAbsolute(sweep_getPos() + angularOffset_deg);
}

int32_t sweep_readIRSensor()
{
    return getADCReading();
}

int32_t sweep_readSonicSensor()
{
    return get_sonic_reading();
}

void sweep_doSweep(float startAngle_deg, float endAngle_deg,
                   float sweepIncrement_deg, uint32_t incDelay_ms) //TODO fix this routine
{
    // If the given sweep increment is zero, return immediately.
    if (!sweepIncrement_deg)
        return;

    // If the specified starting angular position is outside of the valid range [-90,90],  overwrite it with
    // the closest valid angular position.
    if (startAngle_deg > 90)
    {
        startAngle_deg = 90;
    }
    else if (startAngle_deg < -90)
    {
        startAngle_deg = -90;
    }

    // If the specified ending angular position is outside of the valid range [-90,90],  overwrite it with
    // the closest valid angular position.
    if (endAngle_deg > 90)
    {
        endAngle_deg = 90;
    }
    else if (endAngle_deg < -90)
    {
        endAngle_deg = -90;
    }

    // If the given start and end angles are equal, return immediately.
    if (startAngle_deg == endAngle_deg)
        return;

    // Ensure that the given angle increment is positive
    sweepIncrement_deg = fabs(sweepIncrement_deg);
    // The direction of the sweep, represented as either 1 (for a CCW sweep)
    // or -1 (for a CW sweep).
    int8_t sweepDir = (endAngle_deg > startAngle_deg ? 1 : -1);

    // The current distance reported by the IR sensor, in cm
    float current_IR_dist_cm = 0;
    // The current distance reported by the sonic sensor, in cm
    float current_sonic_dist_cm = 0;
    // A 128-bit buffer for character data to be sent over UART
    char text_buffer[128] = { 0 };  // All values initially set to zero

    // The amount of time to wait before getting the next sensor readings
    uint32_t servoWaitTime_millis = 0;

    // Ensure that the servo is at the correct starting position before performing the sweep
    if (sweep_getPos() != startAngle_deg)
    {
        // Move servo to correct position
        servoWaitTime_millis = sweep_moveAbsolute(startAngle_deg);
        // Wait to ensure that the servo has reached the correct position
        timer_waitMillis(servoWaitTime_millis);
    }

    // The angular position of the robot on the previous loop iteration
    float prevPos_deg = startAngle_deg - (sweepDir * sweepIncrement_deg);

    // Perform the sweep in the proper direction using the given increment
    while (((sweepDir >= 0)
            && ((prevPos_deg + sweepIncrement_deg) <= endAngle_deg))
            || ((sweepDir < 0)
                    && ((prevPos_deg - sweepIncrement_deg) >= endAngle_deg)))
    {
        // Wait to ensure that the servo has reached the correct position
        if (servoWaitTime_millis > 0)
            timer_waitMillis(servoWaitTime_millis);
        // Delay the specified amount of time between increments
        if (incDelay_ms > 0)
            timer_waitMillis(incDelay_ms);

        // Start sonic measurement
        sonic_chirp();

        // Read current value of sonic sensor and convert to distance in cm
        current_sonic_dist_cm = sonic_convertToDist(sweep_readSonicSensor());
        // Read current value of IR sensor and convert to distance in cm
        current_IR_dist_cm = ir_convertToDist(sweep_readIRSensor());

        float sweep_offset_deg = 8.0;

        // Format data into a string and send it over UART.
        // Note: Only send an IR sensor reading if the measured distance is less than or equal to 60 cm.
        snprintf(text_buffer, 128, "s %.2f %.2f %.2f\r\n",
                 sweep_getPos() - (sweepDir * sweep_offset_deg),
                 current_sonic_dist_cm,
                 (current_IR_dist_cm > 60 ? 0.0 : current_IR_dist_cm));
        uart_sendStr(text_buffer);

        // Update previous position variable
        prevPos_deg = sweep_getPos();

        // Move the sensor assembly by the specified increment in the
        // correct direction and get the new wait time
        servoWaitTime_millis = sweep_moveRelative(
                sweepDir * sweepIncrement_deg);
    }

    // Wait to ensure that the servo has reached the correct position
    if (servoWaitTime_millis > 0)
    {
        timer_waitMillis(servoWaitTime_millis);
    }

}

void sweep_doFullSweep(float sweepIncrement_deg, uint32_t incDelay_ms)
{
    // If the given sweep increment is zero, return immediately.
    if (!sweepIncrement_deg)
        return;

    // The direction of the sweep, represented as either 1 (for a CCW sweep)
    // or -1 (for a CW sweep).
    int8_t sweepDir = 1;
    // The amount of time to wait before getting the next sensor readings
    uint32_t servoWaitTime_millis = 0;

    // Determine whether the sensor's greatest clockwise or counterclockwise deflection
    // whichever is closest to its current position, then perform a sweep from that
    // deflection to the other maximum deflection.
    if (sweep_getPos() < 0)
    {
        // Move sensor to maximum clockwise deflection
        servoWaitTime_millis = sweep_moveAbsolute(-90.0);
        // Sweep from "negative" side to "positive" side (Counterclockwise)
        sweepDir = 1;
    }
    else
    {
        // Move sensor to maximum counterclockwise deflection
        servoWaitTime_millis = sweep_moveAbsolute(90);
        // Sweep from "positive" side to "negative" side (Clockwise)
        sweepDir = -1;
    }

    // Wait to ensure that the servo has reached the correct position
    if (servoWaitTime_millis > 0)
    {
        timer_waitMillis(servoWaitTime_millis);
    }

    // Perform the sweep routine between the maximum clockwise and counterclockwise
    // deflections of the sensor assembly in the proper direction
    sweep_doSweep(-90.0 * sweepDir, 90.0 * sweepDir, sweepIncrement_deg,
                  incDelay_ms);

}

