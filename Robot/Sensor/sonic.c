/**
 *      @file   sonic.c
 *
 *      @brief  Library of functions for reading the Sonic Sensor
 *
 *      @author Quentin Urbanowicz
 *      @author Rene Chavez
 *      @author Peyton Sher
 *      @author Don Kieu
 *
 *      @date   2018-11-13, 2018-11-27
 */

#include "sonic.h"

/**
 * The speed of sound in centimeters per second
 */
#define V_SOUND_cmps 34000


// -------------------------------- BEGIN GLOBAL VARIABLES -------------------------------- //

// GLOBAL VARIABLE(S) FOR SONIC:
/**
 * Variable that stores the last known sonic reading
 */
volatile unsigned int sonic_timer = 0;

/**
 * Proxy variable to handle the read in case of overflow
 */
volatile unsigned int sonic_read = 0;

/**
 * Variable to check for return pulse
 */
volatile bool sonic_ready = 0;

// -------------------------------- END GLOBAL VARIABLES -------------------------------- //


void sonic_handler()
{

    unsigned int current_reading = TIMER3_TBR_R;
    //Check Flags and reset if not input capture flag
    if (TIMER3_RIS_R & 0xB00)
    {
        TIMER3_ICR_R |= 0xB00;
    }
    else
    {
        //check if the last sonic pulse returned
        if (sonic_ready)
        {
            //Make sure it's a trigger of sonic sensor by checking PB3
            if ((GPIO_PORTB_DATA_R & 0x08))
            {
                sonic_read = current_reading;
            }
            else
            {
                //Read Sonic sensor, if overflow do different calculation
                if (current_reading < sonic_read)
                {
                    sonic_timer = (0xFFFFFF - sonic_read) + current_reading;
                }
                else
                {
                    sonic_timer = current_reading - sonic_read;
                }
                sonic_ready = 0;
            }

        }
        TIMER3_ICR_R |= 0x400;
    }
}

void chirp_init()
{
    GPIO_PORTB_AFSEL_R &= ~0x08; //Disable TIMER3 input
    GPIO_PORTB_DIR_R &= ~0x000000008; //Clear the PB3 direction
    GPIO_PORTB_DIR_R |= 0x000000008; //Set PB3 as output

}

void sonic_start()
{
    GPIO_PORTB_DATA_R &= ~0x08; //Set PB3 to LOW
    GPIO_PORTB_AFSEL_R |= 0x08; //Set PB3 as AFSEL
    GPIO_PORTB_DIR_R &= ~0x000000008; //set PB3 as input
    GPIO_PORTB_PCTL_R |= 0x7000; //set PB3 to Timer 3
    TIMER3_ICR_R |= 0xF00; // Reset the Interrupt flag
    sonic_ready = 1; //Note that pulse has been sent for the handler to wait for it
}

void sonic_init()
{
    SYSCTL_RCGCTIMER_R |= 0x08; //Enable Timer 3
    SYSCTL_RCGCGPIO_R |= 0x02; //Enable port B
    timer_waitMillis(10);
    GPIO_PORTB_DEN_R |= 0x08; //Enable pin 3

    TIMER3_CTL_R &= ~0x101; //Clear timer 3 CTL
    TIMER3_CFG_R &= ~0x007; //Clear timer 3 CFG
    TIMER3_CFG_R |= 0x04;   //Set to 16 bit timer
    TIMER3_TBMR_R &= ~0xFFF; //Clear Timer 3 TBMR
    TIMER3_TBMR_R |= 0x37; //Set to capture mode, Edge Time, and Match interrupt.



    TIMER3_CTL_R |= 0xC00; //Interrupt on both edges
    TIMER3_IMR_R |= 0xF00; //Set interrupt to all mask
    TIMER3_ICR_R |= 0xF00; //Clear all interrupt flags
    NVIC_EN1_R |= (0x1 << 4); //Turn on Timer 3 interrupt

    IntRegister(INT_TIMER3B, sonic_handler); //register TIMER3A interrupt handler
    IntMasterEnable(); //Initialize global interrupts

    TIMER3_CTL_R |= 0x101; //Turn on both A and B timers
}

void sonic_chirp()
{
    chirp_init(); //Turn off Reading mode and set up GPIO for pulse sending
    GPIO_PORTB_DIR_R &= ~0x000000008;//Clear the direction of PB3
    GPIO_PORTB_DIR_R |= 0x000000008;//Set PB3 as output

    GPIO_PORTB_DATA_R |= 0x08; //Set PB3 to HIGH
    timer_waitMicros(25);
    GPIO_PORTB_DATA_R &= ~0x08; //Set PB3 to LOW
    timer_waitMicros(25);
    sonic_start(); //Wait for return pulse

}

int get_sonic_reading()
{
    return sonic_timer; //Raw reading of the timer
}

float sonic_convertToDist(int rawSonicValue)
{
    // Calculate and return the distance to the detected object in centimeters
    return ((rawSonicValue / 16000000.0) * 0.5 * V_SOUND_cmps);
}
