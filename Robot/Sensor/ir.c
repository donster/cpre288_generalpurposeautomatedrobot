/**
 *      @file   ir.c
 *
 *      @brief  Library of functions for reading the IR Sensor
 *
 *      @author Quentin Urbanowicz
 *      @author Rene Chavez
 *      @author Peyton Sher
 *      @author Don Kieu
 *
 *      @date   2018-11-13, 2018-11-27
 */

#include "ir.h"
#include <inc/tm4c123gh6pm.h>
#include "driverlib/interrupt.h"


// -------------------------------- BEGIN GLOBAL VARIABLES -------------------------------- //

// GLOBAL VARIABLE(S) FOR IR:
/** 
 * Voltage conversion array for IR Sensor
 */
double VoltageArray[12] = { .55, .58, .60, .64, .7, .75, .87, 1.01, 1.24, 1.6,
                            2.32, 3.07 };

/**
 * Distance conversion array for IR Sensor
 */
double DistanceArray[12] = { 60, 55, 50, 45, 40, 35, 30, 25, 20, 15, 10, 5 };

/**
 * Raw Data for IR Sensor
 */
volatile int read = 0;

// -------------------------------- END GLOBAL VARIABLES -------------------------------- //


void adc_handler()
{
    read = ADC_read();
    TIMER4_ICR_R = TIMER_ICR_TATOCINT;
}

void adc_init()
{
    SYSCTL_RCGCADC_R = 0x01; // Enable ADC clock
    SYSCTL_RCGCGPIO_R |= 0x02; // Enable ADC GPIO pin
    timer_waitMillis(1);
    GPIO_PORTB_AFSEL_R &= 0xFFFFFFFEF; // Clear ADC pin AFSEL
    GPIO_PORTB_DIR_R &= 0xFFFFFFFEF; // Clear ADC Direction pin
    GPIO_PORTB_DEN_R |= 0x10; // Enable ADC pin in GPIO
    GPIO_PORTB_AFSEL_R |= 0x10; // Enable ADC pin input in GPIO
    GPIO_PORTB_AMSEL_R |= 0x10; // Set pin to ADC mode
    ADC0_ACTSS_R &= 0xFFFFFFFE; // Clear sampler 0

    ADC0_SAC_R &= 0xFFFFFFF8; // Clear hardware sample averaging
    ADC0_SAC_R |= 0x5; // Hardware sample average at 32x

    ADC0_EMUX_R |= 0x00; // Set to default clock
    ADC0_SSMUX0_R |= 0xA; // Set to ADC10
    ADC0_SSCTL0_R |= 0x6; // End and interrupt at the first sample

    TIMER4_CTL_R &= ~(TIMER_CTL_TAEN | TIMER_CTL_TBEN); // Disable timerB to allow us to change settings
    NVIC_EN2_R |= (1 << 6); // Set interrupt at Timer 4

    IntRegister(INT_TIMER4A, adc_handler); // Register TIMER4A interrupt handler
    TIMER4_CTL_R |= (TIMER_CTL_TAEN | TIMER_CTL_TBEN); // Enable TIMER4A & TIMER4B
    TIMER4_ICR_R = TIMER_ICR_TATOCINT;

    ADC0_ACTSS_R |= 0x01;
    ADC0_ISC_R |= 0x01;
}

int ADC_read()
{
    ADC0_ISC_R |= 0x01; // Clear interrupt flag
    ADC0_PSSI_R |= 0x01;// Initiate sampler
    while (ADC0_RIS_R & 0x01) // Wait until interrupt
        ;
    return (ADC0_SSFIFO0_R & 0xFFF); // Return ADC raw value
}

int getADCReading()
{
    return read; // Get the last reading
}

float ir_convertToDist(int rawIRValue)
{
    // Convert digital quantization value for IR sensor back to voltage
    float voltage = rawIRValue * 0.00081;
    // The distance represented by the raw sensor value in cm
    float distance_cm = 0.0;

    // Loop counter variable
    int i = 0;
    // The approximation of the slope being used
    float slope;

    // Use the best voltage-distance slope approximation to convert IR sensor voltage reading to a distance reading in cm
    for (i = 0; i < 11; i++)
    {
        // Use the slope approximation from the portion of the graph that is local to the given voltage value.
        if ((voltage >= VoltageArray[i]) && (voltage <= VoltageArray[i + 1]))
        {
            // Calculate the slope approximation
            slope = (DistanceArray[i + 1] - DistanceArray[i])
                    / (VoltageArray[i + 1] - VoltageArray[i]);
            // Calculate the distance in centimeters using the best slope approximation
            distance_cm = DistanceArray[i] - VoltageArray[i] * slope
                    + voltage * slope;
        }
    }

    // Return the distance reported by the IR sensor in centimeters
    return distance_cm;
}
