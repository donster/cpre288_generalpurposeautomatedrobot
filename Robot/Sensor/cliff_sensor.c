/**
 *      @file   cliff_sensor.c
 *
 *      @brief  Library of functions for reading the cliff sensors and
 *              determining the type of boundary being detected.
 *
 *      @author Quentin Urbanowicz
 *      @author Rene Chavez
 *      @author Peyton Sher
 *      @author Don Kieu
 *
 *      @date   2018-11-13, 2018-11-27
 */

#include "cliff_sensor.h"


uint16_t cs_readSensorRaw(oi_t *self, CliffSensor sensor)
{

    // Update sensor data struct to ensure that data is accurate and up-to-date

    // Return the reading from the indicated cliff sensor
    switch (sensor)
    {
		// Return left cliff sensor reading
		case LEFT:
		{
			return self->cliffLeftSignal;
		}
        // Return front left cliff sensor reading
		case FRONT_LEFT:
		{
			return self->cliffFrontLeftSignal;
		}
        // Return front right cliff sensor reading
		case FRONT_RIGHT:
		{
			return self->cliffFrontRightSignal;
		}
        // Return right cliff sensor reading
		case RIGHT:
		{
			return self->cliffRightSignal;
		}

    }

    // Otherwise, return null
    return '\0';
}

Terrain cs_getTerrain(oi_t *self, CliffSensor sensor)
{

    // Read raw value from indicated cliff sensor
    uint16_t sensor_reading = cs_readSensorRaw(self, sensor);

    // Return the type of terrain under the sensor using the experimentally-determined cliff sensor value ranges
    if (sensor_reading >= 3000)
    {
        // Return Terrain.ZONE to indicate the presence of the highly-reflective target zone
        return ZONE;
    }
    else if (sensor_reading >= 2700)
    {
        // Return Terrain.BOUNDARY to indicate the detection of the white tape boundary of the course
        return BOUNDARY;
    }
    else if (sensor_reading >= 1000)
    {
        // In general, any values on the interval [1000, 2700) are just the regular ground.
        // Return Terrain.GROUND.
        return GROUND;
    }
    else
    {
        // Any values lower than 1000 indicate a hole/cliff. Return Terrain.CLIFF.
        return CLIFF;
    }

}
int checkGroundSensors(oi_t* self)
{

	// Get terrain readings from all four cliff sensors
    Terrain left = cs_getTerrain(self, LEFT);
    Terrain fleft = cs_getTerrain(self, FRONT_LEFT);
    Terrain fright = cs_getTerrain(self, FRONT_RIGHT);
    Terrain right = cs_getTerrain(self, RIGHT);

	// If any sensor detects a cliff, return 1
    if (left == CLIFF || fleft == CLIFF || fright == CLIFF || right == CLIFF)
    {
        return 1;
    }
	// If any sensor detects a boundary, return 2
    else if (left == BOUNDARY || fleft == BOUNDARY || fright == BOUNDARY
            || right == BOUNDARY)
    {
        return 2;
    }
	// If any sensor detects the zone, return 3
    else if (left == ZONE || fleft == ZONE || fright == ZONE || right == ZONE)
    {
        return 3;
    }
	// Otherwise, return 0 to indicate normal ground
    else
    {
        return 0;
    }
}

