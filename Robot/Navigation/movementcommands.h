/**
 *
 *      @file   movementcommands.h
 *
 *      @brief  Header for controlling the robot heading and direction.
 *              Also includes the UART parser for commands.
 *
 *              
 *
 *      @author Quentin Urbanowicz
 *      @author Rene Chavez
 *      @author Peyton Sher
 *      @author Don Kieu
 *      @date   2018-11-13, 2018-11-27
 */
#include <Utils/open_interface.h>
#include <Communication/uart.h>

#ifndef NAVIGATION_MOVEMENTCOMMANDS_H_
#define NAVIGATION_MOVEMENTCOMMANDS_H_

/**
 * Does the process of moving the robot a specific distance
 *
 * @param input
 *        - the given distance to move in mm
 */
void moveRobotDistance(oi_t* self, int input);

/**
 * Does the process of moving the robot a specific distance which ignores the ground sensors and pushes it past the boundaries set.
 *
 * @param input
 *      - the given distance to move in mm
 */
void moveRobotDistanceIgnore(oi_t* self, int input);

/**
 * Turns the robot to a specified degree, 0 being the starting degree
 *
 * @param degrees
 *       -number of degrees wanting to be turned
 *
 * @return
 *        returns the current degrees which helps keep track of where the robot is for the gui
 */

float turnRobotDegree(oi_t* self, float degrees);

/**
 * This parses the command that we send in to the robot to make it drive
 *
 * @param cmdStr- this is the string which has the type of command sent to it which is updated
 * @param type- This is the actual type of command set based on the command enum
 * @param degree- parsed and updated to get the command for the amount of degrees to turn
 * @param distance- parsed and updated to get the command for the amount of distance to travel
 *
 */
void parseCommand(char * cmdStr, int * type, int * degree, int * distance);

/**
 * Gets the position of the bot that we have been keeping track of every movement command
 *
 * @param x- this is the value that is updated for the current x position
 * @param y - this is the value that is updated for the current y position
 * @param d- this is the current heading which is decided by the amount of degrees turned since starting
 *
 */
void getPosition(float * x, float * y, float * d);

/**
 * Enum representing a command sent to the robot over TCP and UART
 */
enum TCP_COMMAND_TYPE
{
    /** Indicates an unknown or malformed command */
    ERROR,
    /** Indicates a movement command */
    MOVE,
    /** Indicates a command to play a sound or song */
    PLAY_SOUND,
    /** Indicates a command to perform a sweep sensor scan */
    SWEEP_SENSOR,
    /** Indicates a query for a position update from the robot */
    QUERY_MOVEMENT,
    /** Indicates a movement command that ignores boundary detection */
    MOVE_IGNORE
};

#endif /* NAVIGATION_MOVEMENTCOMMANDS_H_ */
