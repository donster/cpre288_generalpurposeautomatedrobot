/**
 *
 *      @file   movementcommands.c
 *
 *      @brief  Library of functions for controlling the robot heading and direction. Also includes the UART parser for commands.
 *      @author Quentin Urbanowicz
 *      @author Rene Chavez
 *      @author Peyton Sher
 *      @author Don Kieu
 *      @date   2018-11-13, 2018-11-27
 */
#include "movementcommands.h"
#include <math.h>
#include <Utils/lcd.h>
#include <Sensor/cliff_sensor.h>

/**
 * Local definition of the value of pi
 */
#define PI 3.1415926535897932

// -------------------------------- BEGIN GLOBAL VARIABLES -------------------------------- //

// GLOBAL VARIABLE(S) FOR MOVEMENTCOMMANDS:
/**
 * Heading of the robot in degrees
 */
float heading = 0;

/**
 * X Position in Millimeters
 */
float posx = 0;

/**
 * Y Position in Millimeters
 */
float posy = 0;

/**
 * Steady State Error Boundaries for KP control
 */
float err = 5.0;

/**
 * String to send out for the Robot GUI
 */
char * data;

// -------------------------------- END GLOBAL VARIABLES -------------------------------- //

void moveRobotDistance(oi_t* self, int input)
{
    oi_update(self);
    oi_update(self);

    float current_distance = self->distance; // Set Current distance to the delta distance from last update
    float kp = 5; //KP Gain to 5
    float error = kp * (input - current_distance); //Initialize feedback error
    int groundVar = 0; //Initialize Ground Check
    int lastGround = 0; // Initialize Bumper Check or Cliff check
    //Move until we hit steady state error
    while ((error / kp) > err || (error / kp) < -err)
    {
        oi_update(self);
        //Add distance traveled from last update
        current_distance += self->distance;
        //Set Feedback Error
        error = kp * (input - current_distance);

        groundVar = checkGroundSensors(self);

        /// checks the ground type and sends out a string over uart for the gui to interpret
        if (groundVar == 3)
        {
            snprintf(data, 100, "z %d %d %d %d\0", cs_getTerrain(self, LEFT),
                     cs_getTerrain(self, FRONT_LEFT),
                     cs_getTerrain(self, FRONT_RIGHT),
                     cs_getTerrain(self, RIGHT));
            uart_sendStr(data);
        }
        else if (groundVar == 2)
        {
            oi_setWheels(0, 0);
            snprintf(data, 100, "b %d %d %d %d\0", cs_getTerrain(self, LEFT),
                     cs_getTerrain(self, FRONT_LEFT),
                     cs_getTerrain(self, FRONT_RIGHT),
                     cs_getTerrain(self, RIGHT));
            uart_sendStr(data);
            break;

        }
        else if (groundVar == 1 && lastGround == 0)
        {
            lastGround = 1;
            oi_setWheels(0, 0);
            snprintf(data, 100, "c %d %d %d %d\0", cs_getTerrain(self, LEFT),
                     cs_getTerrain(self, FRONT_LEFT),
                     cs_getTerrain(self, FRONT_RIGHT),
                     cs_getTerrain(self, RIGHT));
            uart_sendStr(data);
            break;
        }

        /// checks if the bump sensor is triggered and sends out a string for the gui to interpret
        if ((self->bumpLeft == 1 || self->bumpRight == 1) && lastGround == 0)
        {
            oi_setWheels(0, 0);
            lastGround = 1;
            snprintf(data, 100, "q %d %d\0", self->bumpLeft, self->bumpRight);
            uart_sendStr(data);
            break;
        }

        /// error calculation updated to stop the movement properly as well as bounding the speed to 150 mm/s
        error = error > 150.0 ? 150.0 : error;
        error = error < -150.0 ? -150.0 : error;
        oi_setWheels((int) error, (int) error);
        snprintf(data, 100, "x %.2f y %.2f h %.2f\0",
                 posx - current_distance * sin(heading * PI / 180.0),
                 posy + current_distance * cos(heading * PI / 180.0), heading);
        uart_sendStr(data);

    }

    posx += -current_distance * sin(heading * PI / 180.0);
    posy += current_distance * cos(heading * PI / 180.0);
    oi_setWheels(0, 0);

}

void moveRobotDistanceIgnore(oi_t* self, int input)
{
    oi_update(self);
    oi_update(self);

    float current_distance = self->distance; // Set Current distance to the delta distance from last update
    float kp = 5; //KP Gain to 5
    float error = kp * (input - current_distance); //Initialize feedback error
    int groundVar = 0; //Initialize Ground Check
    int lastGround = 0; // Initialize Bumper Check or Cliff check
    //Move until we hit steady state error
    while ((error / kp) > err || (error / kp) < -err)
    {
        oi_update(self);
        current_distance += self->distance;
        error = kp * (input - current_distance);

        /// checks the different ground types and sends out a string based on what type of ground is detected, it doesnt stop at them however.
        groundVar = checkGroundSensors(self);
        if (groundVar == 3)
        {
            snprintf(data, 100, "z %d %d %d %d\0", cs_getTerrain(self, LEFT),
                     cs_getTerrain(self, FRONT_LEFT),
                     cs_getTerrain(self, FRONT_RIGHT),
                     cs_getTerrain(self, RIGHT));
            uart_sendStr(data);
        }
        else if (groundVar == 2)
        {
            snprintf(data, 100, "b %d %d %d %d\0", cs_getTerrain(self, LEFT),
                     cs_getTerrain(self, FRONT_LEFT),
                     cs_getTerrain(self, FRONT_RIGHT),
                     cs_getTerrain(self, RIGHT));
            uart_sendStr(data);
        }
        else if (groundVar == 1 && lastGround == 0)
        {
            lastGround = 1;
            oi_setWheels(0, 0);
            snprintf(data, 100, "c %d %d %d %d\0", cs_getTerrain(self, LEFT),
                     cs_getTerrain(self, FRONT_LEFT),
                     cs_getTerrain(self, FRONT_RIGHT),
                     cs_getTerrain(self, RIGHT));
            uart_sendStr(data);
            break;
        }
        /// checks the bump sensor and sends out a command if one is triggered
        if ((self->bumpLeft == 1 || self->bumpRight == 1) && lastGround == 0)
        {
            oi_setWheels(0, 0);
            lastGround = 1;
            snprintf(data, 100, "q %d %d\0", self->bumpLeft, self->bumpRight);
            uart_sendStr(data);
            break;
        }

        /// error calculation updated to stop the movement properly as well as bounding the speed to 150 mm/s
        error = error > 150.0 ? 150.0 : error;
        error = error < -150.0 ? -150.0 : error;
        oi_setWheels((int) error, (int) error);
        snprintf(data, 100, "x %.2f y %.2f h %.2f\0",
                 posx - current_distance * sin(heading * PI / 180.0),
                 posy + current_distance * cos(heading * PI / 180.0), heading);
        uart_sendStr(data);

    }

    posx += -current_distance * sin(heading * PI / 180.0);
    posy += current_distance * cos(heading * PI / 180.0);
    oi_setWheels(0, 0);

}

float turnRobotDegree(oi_t* self, float degrees)
{
    oi_update(self);
    oi_update(self);
    float current_degree = 0;
    float kp = 10;
    float error = kp * (degrees - current_degree);
    float newHeading = heading;
    //Turn until we hit steady state error
    while ((error / kp) > 1.0 || (error / kp) < -1.0)
    {
        //Add delta degrees to degree
        current_degree += self->angle;
        //set the Feedback error
        error = kp * (degrees - current_degree);
        //Bound motor speed to 200 mm/s
        error = error > 200 ? 200 : error;
        error = error < -200 ? -200 : error;

        oi_setWheels(error, -error);
        oi_update(self);
        snprintf(data, 100, "x %.2f y %.2f h %.2f\0", (float) posx,
                 (float) posy, (float) newHeading);
        uart_sendStr(data);
        newHeading = heading + current_degree;
    }

    /// makes sure the new heading is updated properly
    if (heading + current_degree > 360)
    {
        newHeading = heading + current_degree;
        heading = newHeading - 360;
    }
    else if (heading + current_degree < -360)
    {
        newHeading = heading + current_degree;
        heading = newHeading + 360;
    }
    else
    {
        newHeading = heading + current_degree;
        heading = newHeading;
    }

    heading = newHeading;

    oi_setWheels(0, 0);
    oi_update(self);
    return current_degree;
}

void parseCommand(char * cmdStr, int * type, int * degree, int * distance)
{

    ///parses the command sent through uart to determine how what should be done
    /// Expecting command '[Command type] value value value', value is optional for command of 's'
    if (cmdStr[0] == 'm')
    {
        *type = MOVE;
        int i = 0;
        i += 2;
        char * parse;
        parse = strtok(&cmdStr[i], " ");
        *degree = atoi(parse);
        i += strlen(parse);

        parse = &cmdStr[i + 1];
        *distance = atoi(parse);

    }
    else if (cmdStr[0] == 'p')
    {
        *type = PLAY_SOUND;
    }
    else if (cmdStr[0] == 's')
    {
        *type = SWEEP_SENSOR;
    }
    else if (cmdStr[0] == 'r')
    {
        *type = QUERY_MOVEMENT;
    }
    else if (cmdStr[0] == 'f')
    {
        *type = MOVE_IGNORE;
        int i = 0;
        i += 2;
        char * parse;
        parse = strtok(&cmdStr[i], " ");
        *degree = atoi(parse);
        i += strlen(parse);

        parse = &cmdStr[i + 1];
        *distance = atoi(parse);

    }
    else
    {
        *type = ERROR;
    }

}

void getPosition(float * x, float * y, float * d)
{
    *x = posx;
    *y = posy;
    *d = heading;
}
