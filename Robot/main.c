/**
 *      @file   main.c
 *
 *      @brief  Library of functions for reading the cliff sensors and
 *              determining the type of boundary being detected.
 *
 *      @author Quentin Urbanowicz
 *      @author Rene Chavez
 *      @author Peyton Sher
 *      @author Don Kieu
 *
 *      @date   2018-11-06, 11-15, 11-27, 12-04, 12-06
 */

#include <Utils/timer.h>
#include <utils/timer4.h>
#include <Utils/lcd.h>
#include <Utils/open_interface.h>
#include <Navigation/movementcommands.h>
#include <Communication/uart.h>
#include <Utils/Sound.h>
#include <Sensor/sweep_sensor.h>
#include <Sensor/cliff_sensor.h>

/**
 *  Data string used to send data over UART
 */
char * data;

int main(void)
{
    /// all the initialization for every piece we used as well as the different variables needed to make the main run properly.
    clock_timer_init();
    data = malloc(100);
    oi_t *self = oi_alloc();
    lcd_init();
    sweep_init();
    oi_init(self);
    oi_update(self);
    oi_update(self);

    int command = 0;
    int degree = 0;
    int distance = 0;


    /// continually looks for a new command and calls the function depending on what command is found. updates every loop and calls parse command when a new command comes through.
    while (1)
    {
        oi_update(self);
        //Parse the last command line read from the UART
        parseCommand(getLastLine(), &command, &degree, &distance);

        //Check the Ground for ending Zone, and it send it out to the GUI if it does
        if (checkGroundSensors(self) == 3)
        {
            snprintf(data, 100, "z %d %d %d %d\0", cs_getTerrain(self, LEFT),
                     cs_getTerrain(self, FRONT_LEFT),
                     cs_getTerrain(self, FRONT_RIGHT),
                     cs_getTerrain(self, RIGHT));
            uart_sendStr(data);
        }

        if (command == MOVE)
        {
            //robot is turned then moved forward until it hits a boundary, cliff, or bumps something, then sends robot position to gui
            turnRobotDegree(self, degree);
            moveRobotDistance(self, distance);
            uart_clear(); //We read the command, clear it off the UART
            float x, y, h;
            getPosition(&x, &y, &h); //Read the current robot position
            memset(data, 0, 100);
            snprintf(data, 100, "x %.2f y %.2f h %.2f\n\0", x, y, h); //Send the robot position to GUI
            uart_sendStr(data);
        }
        else if (command == MOVE_IGNORE)
        {
            //Same as the MOVE command but with the robot ignoring the boundary
            turnRobotDegree(self, degree);
            moveRobotDistanceIgnore(self, distance);
            uart_clear(); //We read the command, clear it off the UART
            float x, y, h;
            getPosition(&x, &y, &h); //Read the current robot position
            memset(data, 0, 100);
            snprintf(data, 100, "x %.2f y %.2f h %.2f\n\0", x, y, h); //Send the robot position to GUI
            uart_sendStr(data);
        }
        else if (command == PLAY_SOUND)
        {
            playSong();
            uart_clear();//We read the command, clear it off the UART
        }
        else if (command == SWEEP_SENSOR)
        {
            sweep_doFullSweep(1.0, 20);
            uart_clear();//We read the command, clear it off the UART
        }
        else if (command == QUERY_MOVEMENT)
        {
            float x, y, h;
            getPosition(&x, &y, &h);
            snprintf(data, 100, "x %.2f y %.2f h %.2f\n\0", x, y, h);
            uart_sendStr(data);
            uart_clear();//We read the command, clear it off the UART
        }
        timer_waitMillis(10);
    }

}
