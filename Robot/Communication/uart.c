/**
 *      @file   uart.c
 *
 *      @brief  Library used to set up the RS232 connector and WIFI module.
 *				Uses UART1 at 115200 baud (Wi-Fi) or 9600 baud (RS232).
 *
 *      @author Quentin Urbanowicz
 *      @author Rene Chavez
 *      @author Peyton Sher
 *      @author Don Kieu
 *
 *      @date   2018-11-13, 2018-11-27
 */

#include "Communication/uart.h"
#include "Utils/lcd.h"
#include <math.h>

/**
 * The system clock frequency in hertz.
 *
 * This value is used to calculate the
 * Baud Rate Divisor when configuring the UART.
 */
#define SYSTEM_CLOCK_Hz 16000000    // 16 MHz

/**
 * The clock divider being used.
 *
 * This value is used to calculate the
 * Baud Rate Divisor when configuring the UART.
 */
#define CLOCK_DIV 16                // Clock divider being used

/**
 * The baud rate for UART communications.
 *
 * This value is set to 115200 for Wi-Fi communications
 * and 9600 for serial (RS232) communications.
 */
#define BAUD_RATE 115200            // Baud rate for Wi-Fi communications
//#define BAUD_RATE 9600              // Baud rate for serial (RS232) communications

/**
 * The routine used to service interrupts for UART1
 */
void UART1_Handler(void)
{

    char current_letter = 0; //Initialize the last character
    current_letter = uart_receive(); //Receive character in UART
    if (current_letter == '\n')
        uart_read(); //If the last character is return line, send to LastLine and clear buffer
    else
    {
        buffer[strlen(buffer)] = current_letter; //Next character is set to the receive
    }
    UART1_ICR_R |= (0x10); //Reset the UART READ FLAG

}

void uart_read()
{
    memcpy(LastLine, buffer, 100); //Copy buffer to LastLine
    memset(buffer, 0, 100); //clear the buffer

}

void uart_clear()
{
    memset(LastLine, 0, 100); //clear the LastLine
}

void uart_init(void)
{

    buffer = malloc(1024); //Set aside 1024 bytes for the buffer
    memset(buffer, 0, 1024);
    LastLine = (char*) malloc(100); //Set aside 100 bytes for the LastLine
    // Enable & Provide a clock to GPIO Port B in Run mode
    SYSCTL_RCGCGPIO_R |= 0x02;
    // Enable clock to UART1
    SYSCTL_RCGCUART_R |= 0x02;

    timer_waitMillis(1); // Small delay

    // Set Port B pins 0 and 1 to their alternative function
    GPIO_PORTB_AFSEL_R |= 0x03;
    // Enable Rx and Tx:
    // Set Port B pins 0 and 1 to U1Rx and U1Tx, respectively
    GPIO_PORTB_PCTL_R &= 0xFFFFFF00;    //Force 0's at desired locations
    GPIO_PORTB_PCTL_R |= 0x00000011;    // Force 1's at desired locations
    // Enable pins 0 and 1 for Port B
    GPIO_PORTB_DEN_R |= 0x3;
    // Set pin 0 (U1Rx) to input and pin 1 (U1Tx) to output
    GPIO_PORTB_DIR_R &= ~0x2;   // Force 0's at desired locations
    GPIO_PORTB_DIR_R |= 0x2;    // Force 1's at desired locations

    // Calculate Baud Rate Divisor
    // See section 14.3.2 of datasheet (page 896)
    double BRD = SYSTEM_CLOCK_Hz / (double) (CLOCK_DIV * BAUD_RATE);

    double iPart, fPart;

    fPart = modf(BRD, &iPart);

    uint16_t iBRD = iPart;  // Integer part of BRD
    uint16_t fBRD = fPart * 64 + 0.5;   // Fractional part of BRD

    // Disable UART1 for configuration
    UART1_CTL_R &= ~0x1;
    // Set UART1 baud rate
    UART1_IBRD_R = iBRD;
    UART1_FBRD_R = fBRD;
    // Set frame: 8 data bits, 1 stop bit, no parity, no FIFO
    UART1_LCRH_R &= ~0xFF;
    UART1_LCRH_R |= 0x60;
    // Set system clock as UART Baud Clock Source
    UART1_CC_R = 0x0;

    UART1_IM_R &= 0xFFFFE80D; // set relavent values to zero for mask
    UART1_IM_R |= 0x00000010; // set the mask for interrupt on read
    UART1_ICR_R |= (0x10);    //Clear the read flag
    NVIC_EN0_R |= (1 << 6);   //Enable the interrupt for UART1

    IntRegister(INT_UART1, UART1_Handler);

    IntMasterEnable(); //Initialize global interrupts

    // Re-enable UART1
    UART1_CTL_R |= 0x1; // Rx & Tx are enabled on reset

}

void uart_sendChar(char data)
{

    // Wait until there is room to send data
    while ((UART1_FR_R & 0x20))
        ;
    // Send data
    UART1_DR_R = data;

}

char uart_receive(void)
{
    // Wait for data to receive
    while (UART1_FR_R & 0x10)
        ;
    // Mask the 4 error bits to grab only the 8 data bits
    return (UART1_DR_R & 0xFF);
}

void uart_sendStr(const char *data)
{

    // Iterate through the string and send each character until
    // the end of the string (null byte) is reached

    while (*data != '\0')
    {
        // Send the nth char in the string over UART
        uart_sendChar(*data);
        // Move pointer to the next char in the array
        data++;
    }

    // Note: for reference see lcd_puts from lcd.c file
}

void uart_sendStrLen(const char *data, int length)
{
    int i;
    for (i = 0; i < length; ++i)
    {
        uart_sendChar(*data); //Send character
        *data++; //Increment to the next.
    }
}

char * getLastLine()
{

    return LastLine; //Return the Character string
}

