/**
 *      @file   uart.h
 *
 *      @brief  Header file for uart.c
 *
 *              Library used to set up the RS232 connector and WIFI module.
 *				Uses UART1 at 115200 baud (Wi-Fi) or 9600 baud (RS232).
 *
 *      @author Quentin Urbanowicz
 *      @author Rene Chavez
 *      @author Peyton Sher
 *      @author Don Kieu
 *
 *      @date   2018-11-13, 2018-11-27
 */

#ifndef UART_H_
#define UART_H_

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include "Utils/timer.h"

#include <inc/tm4c123gh6pm.h>
#include "driverlib/interrupt.h"

/**
 * Data where the last characters are store
 */
char * buffer;

/**
 * Data where the last parsed line is stored
 */
char * LastLine;

/**
 * Initialize Uart
 */
void uart_init(void);

/**
 * Send one Character
 *
 *  @param data
 *              - Character to send
 */
void uart_sendChar(char data);

/**
 * Receive UART data and copy it into buffer
 */
char uart_receive(void);

/**
 * Send an entire string
 *
 *  @param data
 *              - String to send
 */
void uart_sendStr(const char *data);

/**
 * Send an entire string with specified string length.
 *
 * This function can be used to ensure that only the desired
 * number of characters are sent over UART.
 *
 *  @param data
 *              - String to send
 *  @param length
 *              - Length of the string to send
 */
void uart_sendStrLen(const char *data, int length);

/**
 * Copy the last buffered line to LastLine then clear the buffer
 */
void uart_read();

/**
 * Clear the Last buffered line
 */
void uart_clear();

/**
 * Return the String of the last line read
 */
char * getLastLine();

#endif /* UART_H_ */
