/**
 *      @file	Sound.c
 *
 *		@brief	Library of functions for playing sound.
 *
 *      @author Quentin Urbanowicz
 *      @author Rene Chavez
 *      @author Peyton Sher
 *      @author Don Kieu
 *
 * 		@date 2018-11-27
 */

#include <Utils/open_interface.h>

void playSong()
{
	
	// Array of note pitch values
    unsigned char Notes[16] = { 66, 67, 69, 74, 64, 66, 67, 70, 71, 73, 78, 71,
                                73, 74, 76, 78 };
	// Array of note duration values
    unsigned char Dur[16] = { 8, 8, 16, 72, 8, 8, 64, 8, 8, 8, 72, 8, 8, 32, 32,
                              32 };

	// Load the song
    oi_loadSong(1, 16, Notes, Dur);
    // Play the song
	oi_play_song(1);
	
}
