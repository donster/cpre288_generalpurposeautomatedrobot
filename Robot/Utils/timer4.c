/**
 *      @file   timer4.c
 *
 *      @brief  Library file for handling a timer interrupt for IR Sensor
 *
 *      @author Quentin Urbanowicz
 *      @author Rene Chavez
 *      @author Peyton Sher
 *      @author Don Kieu
 *
 *      @date   2018-11-13, 2018-11-27
 */

#include "timer4.h"

/**
 * The prescaler to be used for TIMER4
 */
#define TIMER4_PRESCALER 250

/**
 * This is the interval for incrementing the clock.
 *
 * 9600 is the required start value of the timer for it to count
 * down to zero in 75 milliseconds.
 */
#define TIMER4A_COUNTS 9600 // Starting value of Timer to countdown to zero in 75 millisecond

/**
 * This is the interval for checking the buttons.
 *
 * 19200 is the required start value of the timer for it to count
 * down to zero in 150 milliseconds.
 */
#define TIMER4B_COUNTS 19200 // Starting value of Timer to countdown to zero in 150 milliseconds

void clock_timer_init(void)
{
    SYSCTL_RCGCTIMER_R |= SYSCTL_RCGCTIMER_R4; // Turn on clock to TIMER4

    // Configure the timer for input capture mode
    TIMER4_CTL_R &= ~(TIMER_CTL_TAEN | TIMER_CTL_TBEN); // Disable Timer4A and Timer4B to allow us to change settings
    TIMER4_CFG_R |= TIMER_CFG_16_BIT; // Set to 16 bit timer

    TIMER4_TAMR_R = TIMER_TAMR_TAMR_PERIOD; // Set for periodic mode, down count
    TIMER4_TBMR_R = TIMER_TBMR_TBMR_PERIOD; // Set for periodic mode, down count

    TIMER4_TAPR_R = TIMER4_PRESCALER - 1; // 75  ms clock
    TIMER4_TBPR_R = TIMER4_PRESCALER - 1;  // 150 ms clock

    TIMER4_TAILR_R = TIMER4A_COUNTS - 1; // set the load value for the  75 ms clock
    TIMER4_TBILR_R = TIMER4B_COUNTS - 1; // set the load value for the 150 ms clock

    // Clear TIMER4B interrupt flags
    TIMER4_ICR_R = (TIMER_ICR_TATOCINT | TIMER_ICR_TBTOCINT); // Clears TIMER4 time-out interrupt flags
    TIMER4_IMR_R |= (TIMER_IMR_TATOIM | TIMER_IMR_TBTOIM); // Enable TIMER4(A&B) time-out interrupts


    TIMER4_CTL_R |= (TIMER_CTL_TAEN | TIMER_CTL_TBEN); // Enable TIMER4A & TIMER4B
}

