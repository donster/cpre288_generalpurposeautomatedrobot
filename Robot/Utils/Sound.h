/**
 *      @file   Sound.h
 *
 *      @brief  Header file for Sound.c
 *
 *              Library of functions for playing sound.
 *
 *      @author Quentin Urbanowicz
 *      @author Rene Chavez
 *      @author Peyton Sher
 *      @author Don Kieu
 *
 * 		@date 2018-11-27
 */

#ifndef UTILS_SOUND_H_
#define UTILS_SOUND_H_

#include <Utils/open_interface.h>
/**
 * Play the Indiana Jones theme song
 */
void playSong();

#endif /* UTILS_SOUND_H_ */
