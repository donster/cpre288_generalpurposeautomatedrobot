/**
 *      @file   timer4.h
 *
 *      @brief  Header file for timer4.c
 *
 *              Library file for handling a timer interrupt for IR Sensor
 *
 *      @author Quentin Urbanowicz
 *      @author Rene Chavez
 *      @author Peyton Sher
 *      @author Don Kieu
 *
 *      @date   2018-11-13, 2018-11-27
 */

#include <stdint.h>
#include <stdbool.h>
#include <inc/tm4c123gh6pm.h>
#include "driverlib/interrupt.h"

#ifndef TIMER4_H_
#define TIMER4_H_

/**
 * Initializes the timer to be used with the IR sensor (TIMER4)
 */
void clock_timer_init(void);

#endif /* TIMER4_H_ */
