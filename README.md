@mainpage
Navigation Robot
@section Navigation Robot
@subsection Introduction
	The purpose of this robot is to navigate to a destination while avoid obstacles.
This robot is controlled through a gui by a remote user. This robot will parse commands
given by the user then will run an action based on what command it has received. It will
then give a response based on the command it had received. The responses that it sends
are its position, sensor data, or obstacle and boundary warnings. If the robot does bump 
into an object, it will stop. Likewise if the robot encounters a boundary or a cliff, it 
will stop and wait for user input. 	

@subsection Robot_Hardware
	This robot is controlled using a Tiva TM4C123GH6PM. It is a modified roomba that has a
servo with an IR and sonic sensor mounted to it. In addition to the mounted sensors, the roomba
sensor contain bump sensors and ground sensors to detect the type of ground the robot is over.
This robot is a nonholonomic system which means the robot cannot 'strafe'.	

@subsection Commands
	To move the robot send :
		
		m delta_heading delta_distance
	
	The robot will turn a delta heading then move in a straight line with a delta distance.
	delta_heading is in degrees with counter clockwise as positive.
	delta_distance is in millimeters
	
	You can also move past boundaries with:
		
		m delta_heading delta_distance
	
	With both movements commands, the robot will output it's position as it is moving.
	
	To get a sensor feed:
		
		s
		
	This is trigger the robot to sweep it's IR and Sonic sensor in a 180 sweep. Either
	clockwise or counter clockwise. The robot will give the sensor angle it is at and the 
	IR and Sonic sensor data in cm.

	To play the Indiana Jones Theme:
	
		p
	
	
@subsection Robot_Responses
	During movement, the robot will send out it's position every loop. The position
	is formated in the example below:
	
		x 100 y 100 h 90
	
	x and y are in mm as the position of the robot from where it started.
	h is the current heading in degrees from where it started.
	
	During movement, if the robot finds an obstacle or bumps into an object,
	the robot will stop moving, send what it has sensed, then output it's
	current location. What it sends out depends on the obstacle. For floor
	obstacles, it will send out:
	
		type left left_front right right_front
	
	where type can only be b, c, z. When the type is b, that's boundary the robot
	is over, c is a cliff, and z is the destination area. left, left_front, right,
	and right_front is the sensor that triggered. These are output as a 1 or 0.
	
	If the response is:
	
		b left_bumper right_bumper
	
	That means the robot bumped into something and it will output which bumper as 
	a 1 or 0 for each bumper.
	
	The output to the sensor data as a scan is being run is given as:
		s angle IR_distance Sonic_Distance
	where angle is the degree that the servo is pointing at.
	IR_Distance is the distance from which the object is sensed.
	Sonic_Distance is the distance from which the object is sensed.
	
@subsection Acknowledgements
	Code that we didn't write:
		
		open_interface.h - Author: Noah Bergman
		open_interface.c - Author: Noah Bergman, Eric Middleton, dmlarson
		timer.h - Author: Eric Middleton
		timer.c - Author: Eric Middleton
		lcd.h - Author: Noah Bergman
		lcd.c - Author: Noah Bergman, Eric Middleton
	
	These have be lifted from the labs.
	We did however, made some modifications to open_interface in order
	to output degrees as a float. 

@subsection Bugs
	
		-The robot currently registers a boundary prior to registering a zone. So
			it will output both at the same time before continuously output the zone. 
	
@section Navigation_Robot_GUI
@subsection Introduction
	In general, the robot can be controlled via putty through the wifi interface.
But this is a better way to control this robot as the GUI can keep track of the 
objects and where the robot has been. In addition, the when the robot takes a 
sensor scan, the GUI will show a radar like pulse that shows the sensor data as it
scans.

@subsection Installation
	This program is dependant on:
		
		Boost
		GLFW
		OpenGL
		GLM
	
	It is highly recommended to use CMake to build the solution files.
@subsection Usage
	The GUI will attempt to connect to 192.168.1.1:288 on startup. If it
	fails a warning will appear in the command prompt. Restart and try again.

	@image html CommandLine.png
	
	Once the GUI successfully connects to the robot, it will initially look like this
	
	@image html RobotStartup.png
	
	The default area that is seen is -500 and 500 millimeters from the robot. Left clicking
	anywhere on this map will move the center of the robot in that direction by turning the
	robot to the direction clicked and moving the robot forward to where the mouse is. To
	force a move past a boundary, use 'f' instead of left clicking. Right clicking will turn 
	the robot to the direction of the mouse.
	
	You can zoom out with '-' or '='. Minimum area zoom is 500mm radius and  '=' will zoom out as far
	as you want in increments of 500mm. 
	
	As the robot moves, it paints the area it has been in blue, at the resolution of robot size.
	If the robot bumps into anything, it will paint a yellow box to where the robot thinks
	where the object is. If it encounters a cliff, the robot will paint a red box. If it is 
	a boundary, then the robot will paint a light blue box. If it is the zone, the robot will
	paint a purple box.
	
	In addition, the user can paint a yellow box for probable bumpable object locations or locations
	to avoid. This can be done by mousing over and pressing 'v'.
	
	To tell the robot to do a sensor sweep, press 's'. The sensor sweep will print small green dots to
	show where a probable object is. It's range is only about 60 centimeters due to accuracy concerns.
	
	To tell the robot to play a song, press 'p'. It's the Indiana Jones Theme.
	
	Example of how the robot operation looks like.
	
	@image html ResizedRealRobotVisionMap.png
	@image html robotvisionmap.png
@subsection Robot_Commands

	Command List
	
		left click: Move Robot
		right click: Turn Robot only
		f: Move Robot, ignore boundary
		v: Place an obstacle
		s: Start Sensor Scan
		p: Play Indiana Jones Theme
		-: Zoom in by 500mm
		=: Zoom out by 500mm

 
@subsection Acknowledgements

As noted, this is created with boost as well as using some example code from
OpenGL as a template. However, the shader loading function is copied from OpenGL
tutorials.

@subsection Bugs

		-Sensor line hasn't been fixed to follow with the robot.
		-When a zone is encountered, it will keep placing a zone until the buffer resets.
		-Sometimes the robot likes to turn with the largest degree.
		-Do not modify window size. It is undefined on how the controls work when window is a 
			different size.
	
	