/**
 *
 *      @file   tcp.h
 *
 *      @brief  Header for processing and communicating with
 *				The robot.
 *
 *              
 *
 *      @author Quentin Urbanowicz
 *      @author Rene Chavez
 *      @author Peyton Sher
 *      @author Don Kieu
 *      @date   2018-11-13, 2018-11-27
 */

#ifndef TCP_H
#define TCP_H
#include <iostream>


#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/thread.hpp>

#include "render.h"

using boost::asio::ip::tcp;



/**
*	@class TCPClient
* 	@brief Class that handles TCP Sockets
*/
class TCPClient : public std::enable_shared_from_this<TCPClient>
{
public:

/**
*Constructor for the TCP Client 
* @param c- Context of the Boost ASIO 
* @param host- String of the host address to connect to
* @param port- String of the port to connect to 
* @param _render- Pointer to the render
*/
	TCPClient(boost::asio::io_context& c,
		const std::string& host,
		const std::string& port,
		Render* _render);

/**
*The string to send to the robot
* @param str- The string to send over TCP
*/	
	void send(std::string& str);
/**
*Function to parse the Command recieved from robot
* @param command- The string of the command
*/		
	void parseCommand(const char* command);
/**
*Function required to start reading data off the TCP
*/	
	void start();
/**
*Function that recieves data from the TCP. Do Not Call Directly.
*/		
	void recieve();
/**
*Deconstructor for TCP. Safely shutdown the TCP connect.
*/		
	~TCPClient();
private:
	boost::asio::io_context& io_context;
	tcp::socket socket;
	tcp::endpoint endpoint;
	boost::array<char, 128>  data;
	Render* render;
	bool _recieve;
	boost::thread* readThread;

};

#endif