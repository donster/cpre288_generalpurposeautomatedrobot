/**
 *      @file   render.cpp
 *
 *      @brief  Library of functions for rendering the robot Vision
 *
 *      @author Quentin Urbanowicz
 *      @author Rene Chavez
 *      @author Peyton Sher
 *      @author Don Kieu
 *
 *      @date   2018-11-13, 2018-11-27
 */

#include "render.h"
#include <boost/thread.hpp>


/** Max width of the robotvision window */
#define MAX_WIDTH 600
/** Max height of the robotvision window */
#define MAX_HEIGHT 600
/** Local definition of the value of pi */
#define PI 3.14159265359
/** The radius of the robot in millimeters */
#define robotRadius 150.0

Render::Render() {
	//Initialize the Window to render our graphics
	initGLFW();

	//Initial Zoom at 500mm radius
	width = 1000;
	height = 1000;
	//Initial Robot Position at 0,0 with heading of 0
	setRobotPosition(0, 0, 0);
	
	// Set the center of the viewport at robot position (0,0)
	View = glm::lookAt(
		robotPosition+glm::vec3(0.0,0.0,1.0),
		robotPosition,
		glm::vec3(0, 1, 0.0)
	);

	//Set the view at Initial Zoom
	Projection = glm::ortho(-width, width, -height, height, -1.0f, 1.0f);

	//Generate and bind a vertex array to the GPU
	glGenVertexArrays(1, &ObstacleArrayID);
	glBindVertexArray(ObstacleArrayID);

	//Generate Drawing Buffers and bind them to the GPU
	glGenBuffers(1, &obstacleBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, obstacleBuffer);

	glGenBuffers(1, &robotHeadingBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, robotHeadingBuffer);

	glGenBuffers(1, &mapID);
	glBindBuffer(GL_ARRAY_BUFFER, mapBuffer);

	glGenBuffers(1, &sonarLineID);
	glBindBuffer(GL_ARRAY_BUFFER, sonarLineBuffer);	

	glGenBuffers(1, &robotBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, robotBuffer);

	//Initialize Vertex Data to draw the robot
	glm::vec3 pos = robotPosition;
	robot.clear();
	robot.push_back(pos);
	for (float i = 0; i <= 20; i++) {
		robot.push_back(pos + glm::vec3(robotRadius * sin(2 * PI*i / 20.0), 
										robotRadius *  cos(2 * PI*i / 20.0), 
										0.0
										)
						);
	}

	//Initialize Vertex Data to draw the header
	float headingRads = heading * PI / 180.0;
	robotHeading.clear();
	robotHeading.push_back(pos + glm::vec3(-robotRadius * sin(headingRads), 
											robotRadius * cos(headingRads),
											0)
						  );

	robotHeading.push_back(pos + glm::vec3(-robotRadius * sin(headingRads + PI / 2), 
											robotRadius * cos(headingRads + PI / 2), 
											0)
						  );

	robotHeading.push_back(robotPosition + glm::vec3(-robotRadius * sin(headingRads - PI / 2),
													  robotRadius * cos(headingRads - PI / 2), 
													  0)
						  );


	//Initialize Vertex Data to draw the Sonar Line
	sonarLineData.push_back(glm::vec4(robotPosition, 2.0));
	sonarLineData.push_back(glm::vec4(robotPosition, 2.0));
	setSonarLine(0.0);

	//Add Vertex Data on the current map location.
	//This specifically makes (0,0) mapped
	addMapLocation();

	//Load shaders to GPU and get the program ID of it's location
	ObstacleID = loadShaders(std::string("./obstacles.vert").c_str(),
		std::string("./obstacles.frag").c_str()
	);

	RobotID = loadShaders(std::string("./robot.vert").c_str(),
		std::string("./robot.frag").c_str()
	);
	
	RobotHeadingID = loadShaders(std::string("./heading.vert").c_str(),
		std::string("./heading.frag").c_str()
	);

	MapProgramID = loadShaders(std::string("./obstacles.vert").c_str(),
		std::string("./obstacles.frag").c_str()
	);

	SonarLineProgramID = loadShaders(std::string("./obstacles.vert").c_str(),
		std::string("./obstacles.frag").c_str()
	);

}

void Render::setSonarLine(float angle) {

	float headingRads = getRobotHeading() * PI / 180.0;
	//Modify the first and second points of the vertex array
	//for the sonic line
	sonarLineData.at(0) = glm::vec4(robotPosition, 2.0);
	sonarLineData.at(1) = glm::vec4(robotPosition + glm::vec3(-600 * sin(headingRads + angle),
															   600 * cos( headingRads + angle),
															   0.0),
															   2.0);
}

void Render::renderSonarLine() {
	//Send Current View Projection to the Shader
	GLuint MatrixID = glGetUniformLocation(SonarLineProgramID, "MVP");
	glm::mat4 Model = glm::mat4(1.0f);
	glm::mat4 mvp = Projection * View * Model;
	glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &mvp[0][0]);
	//Load the vertex data to the GPU
	glBindBuffer(GL_ARRAY_BUFFER, sonarLineBuffer);
	glBufferData(GL_ARRAY_BUFFER, sonarLineData.size() * 4 * sizeof(GL_FLOAT), &(sonarLineData.front()), GL_STATIC_DRAW);

	//Specify the shader program to use 
	glUseProgram(SonarLineProgramID);

	//Start drawing
	glEnableVertexAttribArray(0);
	//Set Line Width to 20 pts
	glLineWidth(20.0);
	//Use the sonar Line buffer
	glBindBuffer(GL_ARRAY_BUFFER, sonarLineBuffer);
	
	glVertexAttribPointer(
		0,                  // attribute 0. 
		4,                  // 4 floats
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
	);
	glDrawArrays(GL_LINES, 0, sonarLineData.size()); //Draw Line
	glDisableVertexAttribArray(0);
}
void Render::addMapLocation() {
	//Get curent robot position
	float x = robotPosition.x ;
	float y = robotPosition.y ;
	//Check where the robot is in the grid with size of the robot.
	int x_check = (robotPosition.x)/ (robotRadius*2) + 0.5;
	int y_check = (robotPosition.y)/ (robotRadius*2) + 0.5;
	if (x < 0) {
		x_check = (robotPosition.x) / (robotRadius * 2) - 0.5;
	}
	if (y < 0) {
		y_check = (robotPosition.y) / (robotRadius * 2) - 0.5;
	}

		//Check if robot has been here before
	if (mapVision.find(std::make_pair(x_check,y_check)) == mapVision.end()) {
		
		//Mark current position on the map
		mapVision[std::make_pair(x_check, y_check)] = true;
		//Add a square to the Map Vertex Data (6 Vertex)
		mapData.push_back(glm::vec4(x_check*(robotRadius * 2) - robotRadius, 
									y_check*(robotRadius * 2) - robotRadius, 
									0.0, 
									0)
						 );
		mapData.push_back(glm::vec4(x_check*(robotRadius * 2) + robotRadius, 
									y_check*(robotRadius * 2) - robotRadius, 
									0.0, 
									0)
						 );
		mapData.push_back(glm::vec4(x_check*(robotRadius * 2) - robotRadius, 
									y_check*(robotRadius * 2) + robotRadius, 
									0.0, 
									0)
						 );
		mapData.push_back(glm::vec4(x_check*(robotRadius * 2) - robotRadius, 
									y_check*(robotRadius * 2) + robotRadius, 
									0.0, 
									0)
						 );
		mapData.push_back(glm::vec4(x_check*(robotRadius * 2) + robotRadius, 
									y_check*(robotRadius * 2) - robotRadius, 
									0.0, 
									0)
						 );
		mapData.push_back(glm::vec4(x_check*(robotRadius * 2) + robotRadius, 
									y_check*(robotRadius * 2) + robotRadius, 
									0.0, 
									0)
						 );
		std::cout << "Found "<< x_check <<" , "<<y_check << std::endl;
	}

}

void Render::renderMap() {
	//Double check if there anything to draw
	if (mapData.size()) {
		//Send Current View Projection to the Shader
		GLuint MatrixID = glGetUniformLocation(MapProgramID, "MVP");

		glm::mat4 Model = glm::mat4(1.0f);
		glm::mat4 mvp = Projection * View * Model;
		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &mvp[0][0]);
		//Load Vertex Data into GPU
		glBindBuffer(GL_ARRAY_BUFFER, mapBuffer);
		glBufferData(GL_ARRAY_BUFFER, mapData.size() * 4 * sizeof(GL_FLOAT), &(mapData.front()), GL_STATIC_DRAW);

		//Set which shader program to use
		glUseProgram(MapProgramID);
		//Draw Triangles (Note: Two Right Triangles make a rectangle)
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, mapBuffer);
		glVertexAttribPointer(
			0,                  // attribute 0.
			4,                  //  4 floats per Vertex
			GL_FLOAT,           // type
			GL_FALSE,           // normalized
			0,                  // stride
			(void*)0            // array buffer offset
		);
		// Draw the triangle !
		glDrawArrays(GL_TRIANGLES, 0, mapData.size()); // Starting from vertex 0; 3 vertices total -> 1 triangle
		glDisableVertexAttribArray(0);
	}
}


Render::~Render() {
	//Delete Every buffer that was generated
	glDeleteBuffers(1, &obstacleBuffer);
	glDeleteBuffers(1, &robotHeadingBuffer);
	glDeleteBuffers(1, &mapID);
	glDeleteBuffers(1, &sonarLineID);
	glDeleteBuffers(1, &robotBuffer);
	glDeleteVertexArrays(1, &ObstacleArrayID);
	glDeleteProgram(ObstacleID);
	glDeleteProgram(RobotID);
	glDeleteProgram(RobotHeadingID);
	glDeleteProgram(MapProgramID);
	glDeleteProgram(SonarLineProgramID);
}

GLFWwindow* Render::getWindow() {
	return window;
}
glm::vec3 Render::getRobotPosition() {
	return robotPosition;
}

float Render::getRobotHeading() {
	return heading;
}
void Render::setRobotPosition(float x, float y, float h) {
	//Set new robot position on map
	robotPosition = glm::vec3(x, y, 0);
	heading = h;
	//Move camera above new robot position
	View = glm::lookAt(
		robotPosition + glm::vec3(0.0, 0.0, 1.0),
		robotPosition,
		glm::vec3(0, 1, 0.0)
	);	
}

void Render::setView(float z) {
	//Increment Viewable area
	width  += z;
	height += z;
	//Make sure we don't go below 500mm view radius
	width = width < 500 ? 500 : width;
	height = height < 500 ? 500 : height;
	//Set Camera Viewport
	Projection = glm::ortho(-width, width, -height, height, -1.0f, 1.0f);
}
void Render::renderRobot(){

	//Resetting the Robot Position Vertex Buffer
	glm::vec3 pos = robotPosition;
	robot.clear();
	//Adding Robot Position Vertex Buffer
	robot.push_back(pos);
	for (float i = 0; i <= 20; i++) {
		robot.push_back(pos + glm::vec3(robotRadius * sin(2 * PI*i / 20.0), robotRadius *  cos(2 * PI*i / 20.0), 0.0));
	}
	//Setting up View in GPU
	GLuint MatrixID = glGetUniformLocation(RobotID, "MVP");

	glm::mat4 Model = glm::mat4(1.0f);
	glm::mat4 mvp = Projection * View * Model;
	glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &mvp[0][0]);

	//Sending Vertex Data to GPU
	glBindBuffer(GL_ARRAY_BUFFER, robotBuffer);
	glBufferData(GL_ARRAY_BUFFER, robot.size() * 3 * sizeof(GL_FLOAT), &(robot.at(0)), GL_STATIC_DRAW);

	//Using RobotID shader program
	glUseProgram(RobotID);

	//Render Robot as Triangle Fan
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, robotBuffer);
	glVertexAttribPointer(
		0,                  // attribute 0.
		3,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
	);
	// Draw the triangle !
	glDrawArrays(GL_TRIANGLE_FAN, 0, robot.size()); // Starting from vertex 0; 3 vertices total -> 1 triangle
	glDisableVertexAttribArray(0);

}

void Render::addObstacles(float x, float y, float type) {
	//Make sure we don't overflow the data buffer
	if (obstacles.size() > 180 * 6 * 6) {
		//Delete the first 180 points if we overflow
		obstacles.erase(obstacles.begin(), obstacles.begin() + 180 * 4 * 6);
	}

	//Add an obstacle based on position arguments and the type
	x += robotPosition.x;
	y += robotPosition.y;
	obstacles.push_back(glm::vec4(x - 100, y - 100, 0.0, type));
	obstacles.push_back(glm::vec4(x + 100, y - 100, 0.0, type));
	obstacles.push_back(glm::vec4(x - 100, y + 100, 0.0, type));
	obstacles.push_back(glm::vec4(x - 100, y + 100, 0.0, type));
	obstacles.push_back(glm::vec4(x + 100, y - 100, 0.0, type));
	obstacles.push_back(glm::vec4(x + 100, y + 100, 0.0, type));

}

void Render::addSensors(float x, float y) {
	//Make sure we don't overflow the data buffer
	if (sensorData.size() > 180 * 6 * 6) {
		//Delete the first 180 points if we overflow
		sensorData.erase(sensorData.begin(), sensorData.begin() + 180 * 3 * 6);
	}

	//Add an Sensor Data point 
	x += robotPosition.x;
	y += robotPosition.y;
	sensorData.push_back(glm::vec4(x - 5, y - 5, 0.0, 2));
	sensorData.push_back(glm::vec4(x + 5, y - 5, 0.0, 2));
	sensorData.push_back(glm::vec4(x - 5, y + 5, 0.0, 2));
	sensorData.push_back(glm::vec4(x - 5, y + 5, 0.0, 2));
	sensorData.push_back(glm::vec4(x + 5, y - 5, 0.0, 2));
	sensorData.push_back(glm::vec4(x + 5, y + 5, 0.0, 2));

}

void Render::renderObstacles() {
	//Before rendering, make sure we have any obstacles to render
	if (obstacles.size()) {
		//Set view to GPU
		GLuint MatrixID = glGetUniformLocation(ObstacleID, "MVP");

		glm::mat4 Model = glm::mat4(1.0f);
		glm::mat4 mvp = Projection * View * Model;
		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &mvp[0][0]);

		//Send Obstacle Vertex Data to GPU
		glBindBuffer(GL_ARRAY_BUFFER, obstacleBuffer);
		glBufferData(GL_ARRAY_BUFFER, obstacles.size() * 4 * sizeof(GL_FLOAT), &(obstacles.front()), GL_STATIC_DRAW);

		//Use ObstacleID shader program
		glUseProgram(ObstacleID);
		//Render Obstacles
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, obstacleBuffer);
		glVertexAttribPointer(
			0,                  // attribute 0.
			4,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
		);
		// Draw the triangle !
		glDrawArrays(GL_TRIANGLES, 0, obstacles.size()); // Starting from vertex 0; 3 vertices total -> 1 triangle
		glDisableVertexAttribArray(0);
	}
}

void Render::renderSensors() {
	//Check if we have any sensor data to render
	if (sensorData.size()) {
		//Set view to GPU
		GLuint MatrixID = glGetUniformLocation(sensorID, "MVP");

		glm::mat4 Model = glm::mat4(1.0f);
		glm::mat4 mvp = Projection * View * Model;
		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &mvp[0][0]);

		//Send vertex data to GPU
		glBindBuffer(GL_ARRAY_BUFFER, sensorBuffer);
		glBufferData(GL_ARRAY_BUFFER, sensorData.size() * 4 * sizeof(GL_FLOAT), &(sensorData.front()), GL_STATIC_DRAW);

		//Use the sensorID program
		glUseProgram(sensorID);

		//Render sensor data
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, sensorBuffer);
		glVertexAttribPointer(
			0,                  // attribute 0.
			4,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
		);
		// Draw the triangle !
		glDrawArrays(GL_TRIANGLES, 0, sensorData.size()); // Starting from vertex 0; 3 vertices total -> 1 triangle
		glDisableVertexAttribArray(0);
	}
}

void Render::renderHeading() {
		//Reset Heading Arrow
		glm::vec3 pos = robotPosition;
		float headingRads = heading * PI / 180.0;
		robotHeading.clear();
		//Load Heading Arrow Vertex Data into buffer based on current heading
		robotHeading.push_back(pos + glm::vec3(-robotRadius * sin(headingRads), robotRadius * cos(headingRads), 0));
		robotHeading.push_back(pos + glm::vec3(-robotRadius * sin(headingRads + PI / 2), robotRadius * cos(headingRads + PI / 2), 0));
		robotHeading.push_back(robotPosition + glm::vec3(-robotRadius * sin(headingRads - PI / 2), robotRadius * cos(headingRads - PI / 2), 0));

		//Send View to GPU
		GLuint MatrixID = glGetUniformLocation(RobotHeadingID, "MVP");

		glm::mat4 Model = glm::mat4(1.0f);
		glm::mat4 mvp = Projection * View * Model;
		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &mvp[0][0]);

		//Send Heading Vertex Data to GPU
		glBindBuffer(GL_ARRAY_BUFFER, robotHeadingBuffer);
		glBufferData(GL_ARRAY_BUFFER, robotHeading.size() * 3 * sizeof(GL_FLOAT), &(robotHeading.at(0)), GL_STATIC_DRAW);

		//Use RobotHeadingID buffer
		glUseProgram(RobotHeadingID);

		//Render the One triangle for Heading Arrow
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, robotHeadingBuffer);
		glVertexAttribPointer(
			0,                  // attribute 0.
			3,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
		);
		// Draw the triangle !
		glDrawArrays(GL_TRIANGLES, 0, robotHeading.size()); // Starting from vertex 0; 3 vertices total -> 1 triangle
		glDisableVertexAttribArray(0);

}

int Render::initGLFW() {

	if (!glfwInit()) {
		std::cout << "Failed to initalize GLFW" << std::endl;
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);


	window = glfwCreateWindow(MAX_WIDTH, MAX_HEIGHT, "RobotVision", NULL, NULL);

	if (!window) {
		std::cout << "Failed to open GLFW window." << std::endl;
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);

	glewExperimental = true;
	if (glewInit() != GLEW_OK) {
		std::cout << "failed to initalize GLEW" << std::endl;
		return -1;
	}

	glfwSetInputMode(window, GLFW_STICKY_KEYS, GLFW_FALSE);
	return 0;
}

GLuint Render::loadShaders(const char * vertex_file_path, const char * fragment_file_path) {

	// Create the shaders
	GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

	// Read the Vertex Shader code from the file
	std::string VertexShaderCode;
	std::ifstream VertexShaderStream(vertex_file_path, std::ios::in);
	if (VertexShaderStream.is_open()) {
		std::stringstream sstr;
		sstr << VertexShaderStream.rdbuf();
		VertexShaderCode = sstr.str();
		VertexShaderStream.close();
	}
	else {
		printf("Impossible to open %s. Are you in the right directory ? Don't forget to read the FAQ !\n", vertex_file_path);
		getchar();
		return 0;
	}

	// Read the Fragment Shader code from the file
	std::string FragmentShaderCode;
	std::ifstream FragmentShaderStream(fragment_file_path, std::ios::in);
	if (FragmentShaderStream.is_open()) {
		std::stringstream sstr;
		sstr << FragmentShaderStream.rdbuf();
		FragmentShaderCode = sstr.str();
		FragmentShaderStream.close();
	}

	GLint Result = GL_FALSE;
	int InfoLogLength;


	// Compile Vertex Shader
	printf("Compiling shader : %s\n", vertex_file_path);
	char const * VertexSourcePointer = VertexShaderCode.c_str();
	glShaderSource(VertexShaderID, 1, &VertexSourcePointer, NULL);
	glCompileShader(VertexShaderID);

	// Check Vertex Shader
	glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if (InfoLogLength > 0) {
		std::vector<char> VertexShaderErrorMessage(InfoLogLength + 1);
		glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
		printf("%s\n", &VertexShaderErrorMessage[0]);
	}



	// Compile Fragment Shader
	printf("Compiling shader : %s\n", fragment_file_path);
	char const * FragmentSourcePointer = FragmentShaderCode.c_str();
	glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer, NULL);
	glCompileShader(FragmentShaderID);

	// Check Fragment Shader
	glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if (InfoLogLength > 0) {
		std::vector<char> FragmentShaderErrorMessage(InfoLogLength + 1);
		glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
		printf("%s\n", &FragmentShaderErrorMessage[0]);
	}



	// Link the program
	printf("Linking program\n");
	GLuint ProgramID = glCreateProgram();
	glAttachShader(ProgramID, VertexShaderID);
	glAttachShader(ProgramID, FragmentShaderID);
	glLinkProgram(ProgramID);

	// Check the program
	glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
	glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if (InfoLogLength > 0) {
		std::vector<char> ProgramErrorMessage(InfoLogLength + 1);
		glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
		printf("%s\n", &ProgramErrorMessage[0]);
	}


	glDetachShader(ProgramID, VertexShaderID);
	glDetachShader(ProgramID, FragmentShaderID);

	glDeleteShader(VertexShaderID);
	glDeleteShader(FragmentShaderID);

	return ProgramID;
}
