cmake_minimum_required(VERSION 2.8 FATAL_ERROR)
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake_modules/")

project(MAIN)

set(Boost_USE_STATIC_LIBS   ON)

list(APPEND CMAKE_PREFIX_PATH "C:/Program Files/glew")

find_package(OpenGL)
find_package(GLEW)
find_package(GLFW)
find_package(GLM)
find_package(Boost 1.68 COMPONENTS system threads)

include_directories(${GLFW_INCLUDE_DIR}
					${GLM_INCLUDE_DIR}
					${OPENGL_INCLUDE_DIR}
					${GLEW_INCLUDE_DIRS}
					${Boost_INCLUDE_DIRS}
					)
link_directories(${Boost_LIBRARY_DIRS}
				)
add_executable(MAIN tcp.h 
					tcp.cpp 
					render.h 
					render.cpp 
					main.cpp 
					shaders/heading.frag
					shaders/heading.vert
					shaders/robot.frag
					shaders/robot.vert
					shaders/obstacles.frag
					shaders/obstacles.vert
					)
target_link_libraries(MAIN	${GLFW_LIBRARY}
							${GLM_LIBRARY}
							${OPENGL_LIBRARY}
							${GLEW_LIBRARIES}
							${Boost_LIBRARIES}
							)

install (TARGETS MAIN
		DESTINATION 
				bin)

install (FILES
				shaders/heading.frag
				shaders/heading.vert
				shaders/robot.frag
				shaders/robot.vert
				shaders/obstacles.frag
				shaders/obstacles.vert
		DESTINATION 
				bin)
