/**
 *      @file   render.h
 *
 *      @brief  Header Library for rendering the robot Vision
 *       
 *
 *      @author Quentin Urbanowicz
 *      @author Rene Chavez
 *      @author Peyton Sher
 *      @author Don Kieu
 *
 *      @date   2018-11-13, 2018-11-27
 */


#ifndef RENDER_H
#define RENDER_H

#define GLEW_STATIC
#include <GL/glew.h>

#include <gl/GL.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>
#include <vector>
#include <sstream>
#include <fstream>
#include <streambuf>
#include <map>


/**
*	@class Render
* 	@brief Class that handles Drawing the Graphics
*/
class Render {
public:
/**
 *Main Constructor that Handles the Rendering
 */
	Render();
/**
 *Main deconstructor that Handles the Rendering
 */	
	~Render();
/**
 *width of the viewport window
 */	
	float width;
/**
 *height of the viewport window
 */	
	float height;
/**
 *Initialize GLFW
 *@return Returns a -1 on failure, 0 on success.
 */	
	int initGLFW();

/**
 *Add Obstacle on the map into OGL buffer. Color is based on what type of obstacle.
 * @param x- X Position of the obstacle relative to the center of the robot
 * @param y- Y Position of the obstacle relative to the center of the robot
 * @param type- The type of object. 1 == hole, 2 == Sensor Data, 3 == Boundary, 
 *				4 == Bump, 5 == Finish Zone, 0 == Map Visited
 */	
	void addObstacles(float x, float y, float type);
/**
 *Add Sensor data into OGL buffer* 
 * @param x- X Position of the sensor data relative to the center of the robot
 * @param y- Y Position of the sensor data relative to the center of the robot
 */	
	void addSensors(float x, float y);
/**
 *Render sensor data in OGL buffer
 */	
	void renderSensors();
/**
 *Move the viewport right on top of robot position and 
 *modify how much of the area we can see around the robot
 * @param z- How far to zoom out the camera
 */	
	void setView(float z);
/**
 *Render the robot
 */
	void renderRobot();
/**
 *Render the robot heading
 */		
	void renderHeading();
/**
 *Set the position of the robot in the GUI map
 * @param x- X Position of the robot relative to where it started in mm
 * @param y- Y Position of the robot relative to where it started in mm
 * @param h- Heading of the robot relative to where it started in degrees
 */	
	void setRobotPosition(float x, float y, float h);
/**
 *Get current GUI robot Position
 *@return Returns a vec3 of the position. Note that z is zero because 2D plane.
 */	
	glm::vec3 getRobotPosition();
/**
 *Get current GUI robot heading
 *@return Returns the robot heading in degrees.
 */	
	float getRobotHeading();
/**
 *Render the Obstacles Buffer
 */		
	void renderObstacles();
/**
 *Add a visited location into the Robot Map
 */		
	void addMapLocation();
/**
 *Render the map that we have visited
 */
	void renderMap();
/**
 * Get the handler instanced window of GLFW
 * @return Returns the Handlering of the instance window of GLFW
 */
	GLFWwindow* getWindow();
/**
 * Load a shader file and return GLuint of the program ID
 * @param vertex_file_path- String of the path to vertex shader location
 * @param fragment_file_path- String of teh path to fragment shader location
 * @return Returns the Program ID handle that the shaders are loaded to.
 */	
	GLuint Render::loadShaders(const char * vertex_file_path, const char * fragment_file_path);
/**
 * Set the sonar line that shows the current sonic sensor angle
 * @param angle- Angle at which the sonar line is drawn relative to the robot
 */	
	void setSonarLine(float angle);
/**
 * Render the sonar line
 */	
	void renderSonarLine();
private:

	GLFWwindow* window;
	std::vector<glm::vec4> obstacles;
	std::vector<GLuint> obstacles_buffer_array;
	GLuint obstacleBuffer;
	GLuint ObstacleID;
	GLuint ObstacleArrayID;
	GLuint MapProgramID;
	GLuint SonarLineProgramID;

	
	GLuint RobotID;
	GLuint robotBuffer;
	std::vector<glm::vec3> robot;

	GLuint RobotHeadingID;
	GLuint robotHeadingBuffer;
	std::vector<glm::vec3> robotHeading;

	glm::vec3 robotPosition;
	float heading;

	GLuint mapID;
	GLuint mapBuffer;
	std::vector<glm::vec4> mapData;
	std::map<std::pair<int, int>, bool> mapVision;

	GLuint sensorID;
	GLuint sensorBuffer;
	std::vector<glm::vec4> sensorData;

	GLuint sonarLineID;
	GLuint sonarLineBuffer;
	std::vector<glm::vec4> sonarLineData;

	glm::mat4 View;
	glm::mat4 Projection;

};
#endif