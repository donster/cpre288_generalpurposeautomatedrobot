/**
 *
 *      @file   tcp.cpp
 *
 *      @brief  Library of functions for processing and communicating with the robot.
 *
 *              
 *				
 *
 *      @author Quentin Urbanowicz
 *      @author Rene Chavez
 *      @author Peyton Sher
 *      @author Don Kieu
 *      @date   2018-11-13, 2018-11-27
 */

#include "tcp.h"

/** Local definition of the value of pi */
#define PI 3.14159265359
/** The radius of the robot in millimeters */
#define robotRadius 150.0

using boost::asio::ip::tcp;

TCPClient::TCPClient(boost::asio::io_context& c,
	const std::string& host,
	const std::string& port,
	Render* _render)
	:
	io_context(c),
	socket(c),
	render(_render)
{
	//Resolve the TCP endpoint
	tcp::resolver resolver(io_context);
	tcp::resolver::query query(tcp::v4(), host, port);
	//Reset our receive data buffer to 0
	memset(data.data(), 0, 128);
	try {
		//Attempt to Connect to TCP endpoint
		boost::asio::connect(socket, resolver.resolve(host, port));
		_recieve = 1;
		socket.async_read_some(boost::asio::buffer(data, 128),
			boost::bind(&TCPClient::recieve, this)
		);
		boost::thread test = boost::thread(boost::bind(&TCPClient::start, this));
		render->addObstacles(-10, 10, 0);
	}
	catch (std::exception &e) {
		//Send Connection error to command window
		std::cout << "Server Connection failed" << std::endl;
	}
}

void TCPClient::start() {
	//This is here to start the reading process for async receieve
	//Must be run on another thread, this is a blocking call.
	io_context.run();
}

void TCPClient::send(std::string& str) {
	//Send String to TCP endpoint
	socket.send(boost::asio::buffer(str+"\r\n\0", str.size()+5));
}


void TCPClient::parseCommand(const char * command) {
	//Initialize Variables for token parsing
	std::stringstream line_check(command);
	std::string token_check;
	
	//Get the heading for future calculations
	float robotHeading = render->getRobotHeading()*3.1514 / 180.0;
	
	//Flags to check if we have complete heading data
	bool x_flag, y_flag, h_flag;
	x_flag = y_flag = h_flag = 0;
	float x, y, h;
	x = y = h = 0;

	//Check space delimited command line parse
	while (std::getline(line_check,token_check, ' ')) {
		//Look at the first character of the command string
		char command_check = token_check.c_str()[0];
		//Initialize the proxy variable to check the command parse
		std::string s_data;
		
		//Check current robot characteristics for future calculations
		float heading = 0;
		float direction_ir = 0;
		float direction_sonic = 0;
		float prev_x = render->getRobotPosition().x;
		float prev_y = render->getRobotPosition().y;
		
		//Variable to load to check cliff or ground sensors
		int bounds[4];


		//Expected receive commands are as follows:
		//c = Hole
		//b = boundary
		//q = bump
		//z = zone
		//s = sensors
		//x = X Position of Robot in mm
		//y = Y Position of Robot in mm
		//h = Heading of the Robot in Degrees
		//Each command will parse the numbers as floats for position, heading, and sensor. 
		//Ints for everything else
		switch (command_check) {
			case 'x':
				x_flag = 1;
				//Next space delimited is data
				std::getline(line_check, s_data, ' ');
				x = atof(s_data.c_str());
				break;
			case 'y':
				y_flag = 1;
				//Next space delimited is data
				std::getline(line_check, s_data, ' ');
				y = atof(s_data.c_str());
				break;
			case 'h':
				h_flag = 1;
				//Next space delimited is data
				std::getline(line_check, s_data, ' ');
				h = atof(s_data.c_str());
				break;
			case 's':
				// Output Sensor command for debugging and visual purposes
				std::cout << command << std::endl;
				//Next three space delimited is data
				std::getline(line_check, s_data, ' ');
				heading = atof(s_data.c_str())*3.1415/180.0;
				std::getline(line_check, s_data, ' ');
				direction_ir = atof(s_data.c_str());
				std::getline(line_check, s_data, ' ');
				direction_sonic = atof(s_data.c_str());
				render->setSonarLine(heading);
				if (fabsf(direction_ir - direction_sonic) < 10.0) {
					render->addSensors((-(direction_ir+4) * sin(heading + robotHeading) - 10*sin(robotHeading))* 10, 
									   ((direction_ir+4)*cos(heading + robotHeading) + 10 * cos(robotHeading)) * 10
									  );
				}
				break;
			case 'c':
				//Next 4 space delimited is data
				std::getline(line_check, s_data, ' ');
				bounds[0] = atoi(s_data.c_str())*3.1415 / 180.0;
				std::getline(line_check, s_data, ' ');
				bounds[1] = atoi(s_data.c_str());
				std::getline(line_check, s_data, ' ');
				bounds[2] = atoi(s_data.c_str());
				std::getline(line_check, s_data, ' ');
				bounds[3] = atoi(s_data.c_str());
				//Output if there is a cliff
				if (bounds[0] == 0) {
					render->addObstacles(-robotRadius * sin(1.396 / 4 + robotHeading), robotRadius*cos(1.396 / 4 + robotHeading), 1);
					std::cout << "Left Bound" << std::endl;
				}
				else if (bounds[1] == 0) {
					render->addObstacles(-robotRadius * sin(0.175 / 4 + robotHeading), robotRadius*cos(0.175 / 4 + robotHeading), 1);
					std::cout << "Left Front Bound" << std::endl;
				}
				else if (bounds[2] == 0) {
					render->addObstacles(-robotRadius * sin(-0.175 / 4 + robotHeading), robotRadius*cos(-0.175 / 4 + robotHeading), 1);
					std::cout << "Right Front Bound" << std::endl;
				}
				else if (bounds[3] == 0) {
					render->addObstacles(-robotRadius * sin(-1.396 / 4 + robotHeading), robotRadius*cos(-1.396 / 4 + robotHeading), 1);
					std::cout << "Right Bound" << std::endl;
				}
				break;
			case 'b':
				//Next 4 space delimited is data
				std::getline(line_check, s_data, ' ');
				bounds[0] = atoi(s_data.c_str());
				std::getline(line_check, s_data, ' ');
				bounds[1] = atoi(s_data.c_str());
				std::getline(line_check, s_data, ' ');
				bounds[2] = atoi(s_data.c_str());
				std::getline(line_check, s_data, ' ');
				bounds[3] = atoi(s_data.c_str());
				//Output if we hit a boundary
				if (bounds[0] == 3) {
					render->addObstacles(-robotRadius * sin(1.396 / 4 + robotHeading), robotRadius*cos(1.396 / 4 + robotHeading), 3);
					std::cout << "Left Bound" << std::endl;
				}
				else if (bounds[1] == 3) {
					render->addObstacles(-robotRadius * sin(0.175 / 4 + robotHeading), robotRadius*cos(0.175 / 4 + robotHeading), 3);
					std::cout << "Left Front Bound" << std::endl;
				}
				else if (bounds[2] == 3) {
					render->addObstacles(-robotRadius * sin(-0.175 / 4 + robotHeading), robotRadius*cos(-0.175 / 4 + robotHeading), 3);
					std::cout << "Right Front Bound" << std::endl;
				}
				else if (bounds[3] == 3) {
					render->addObstacles(-robotRadius * sin(-1.396 / 4 + robotHeading), robotRadius*cos(-1.396 / 4 + robotHeading), 3);
					std::cout << "Right Bound" << std::endl;
				}
				break;
			case 'q':
				//Next 2 space delimited is data
				int bounds[4];
				std::getline(line_check, s_data, ' ');
				bounds[0] = atoi(s_data.c_str());
				std::getline(line_check, s_data, ' ');
				bounds[1] = atoi(s_data.c_str());
				//Output if it is a bump
				if (bounds[0] == 1) {
					render->addObstacles(-robotRadius*sin(3.1415 / 4 + robotHeading), robotRadius*cos(3.1415 / 4 + robotHeading), 4);
					std::cout << "Bump Left" << std::endl;
				}
				else if (bounds[1] == 1) {
					std::cout << "Bump Right" << std::endl;
					render->addObstacles(-robotRadius*sin(-3.1415 / 4 + robotHeading), robotRadius*cos(-3.1415 / 4 + robotHeading), 4);
				}
				break;
			case 'z':
				//Next 4 space delimited is data
				std::getline(line_check, s_data, ' ');
				bounds[0] = atoi(s_data.c_str())*3.1415 / 180.0;
				std::getline(line_check, s_data, ' ');
				bounds[1] = atoi(s_data.c_str());
				std::getline(line_check, s_data, ' ');
				bounds[2] = atoi(s_data.c_str());
				std::getline(line_check, s_data, ' ');
				bounds[3] = atoi(s_data.c_str());
				//Output if it is a zone
				if (bounds[0] == 2) {
					render->addObstacles(-robotRadius * sin(1.396 / 4 + robotHeading), robotRadius*cos(1.396 / 4 + robotHeading), 5);
					std::cout << "Left Bound" << std::endl;
				}
				else if (bounds[1] == 2) {
					render->addObstacles(-robotRadius * sin(0.175 / 4 + robotHeading), robotRadius*cos(0.175 / 4 + robotHeading), 5);
					std::cout << "Left Front Bound" << std::endl;
				}
				else if (bounds[2] == 2) {
					render->addObstacles(-robotRadius * sin(-0.175 / 4 + robotHeading), robotRadius*cos(-0.175 / 4 + robotHeading), 5);
					std::cout << "Right Front Bound" << std::endl;
				}
				else if (bounds[3] == 2) {
					render->addObstacles(-robotRadius * sin(-1.396 / 4 + robotHeading), robotRadius*cos(-1.396 / 4 + robotHeading), 5);
					std::cout << "Right Bound" << std::endl;
				}

				break;
			default:
				break;
		}
		
		//If we manage to hit all x, y, and h commands at this while loop
		if (x_flag && y_flag && h_flag) {
			//Update Robot Position
			render->setRobotPosition(x, y, h);
			render->addMapLocation();
		}


	}


}

void TCPClient::recieve()
{
		//Parse the command recieved from Robot
		parseCommand(data.data());
		//Reset Data
		memset(data.data(), 0, 128);
		//Listen for the next command
		socket.async_receive(boost::asio::buffer(data, 128),0, boost::bind(&TCPClient::recieve, this));

}




TCPClient::~TCPClient() {
	//Stop running the Async Commands
	io_context.stop();
	//Shutdown the socket
	socket.close();
}