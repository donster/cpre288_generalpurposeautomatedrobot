#version 330 core

// Input vertex data, different for all executions of this shader.
layout(location = 0) in vec4 vertexPosition_modelspace;
out vec3 out_color;

uniform mat4 MVP;


void main(){

	if(vertexPosition_modelspace.w > .5 && vertexPosition_modelspace.w < 1.5 ){
		out_color = vec3(1.0,0.0,0.0); //Hole
	}
	else if(vertexPosition_modelspace.w > 1.5 && vertexPosition_modelspace.w < 2.5 ){
		out_color = vec3(0.0,1.0,0.0); //Sensor Radar
	}
	else if(vertexPosition_modelspace.w > 2.5 && vertexPosition_modelspace.w < 3.5 ){
		out_color = vec3(0.0,1.0,1.0); //Boundary 
	}
	else if(vertexPosition_modelspace.w > 3.5 && vertexPosition_modelspace.w < 4.5 ){
		out_color = vec3(1.0,1.0,0.0); //Bump
	}
		else if(vertexPosition_modelspace.w > 4.5 && vertexPosition_modelspace.w < 5.5 ){
		out_color = vec3(1.0,0.0,1.0); //zone
	}
	else{
		out_color = vec3(0.0,0.0,1.0);
	}
    gl_Position = MVP*vec4(vertexPosition_modelspace.xyz,1.0);
    gl_Position.w = 1.0;
	
}