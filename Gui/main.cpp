/**
 *      @file   main.cpp
 *
 *      @brief  GUI main loop
 *
 *      @author Quentin Urbanowicz
 *      @author Rene Chavez
 *      @author Peyton Sher
 *      @author Don Kieu
 *
 *      @date   2018-11-13, 2018-11-27
 */
 
#include "render.h"

#define GLEW_STATIC
#include <GL/glew.h>

#include <gl/GL.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>
#include <vector>
#include <sstream>
#include <fstream>
#include <streambuf>


#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <boost/bind.hpp>
#include <boost/thread.hpp>
#include <boost/thread.hpp>

#include "tcp.h"

/**
 * The main loop for the robotvision GUI
 */
int main(int argc, char * argv[]){

	//Start the renderer
	Render* render = new Render();

	//Start TCP Internet
	boost::asio::io_context context;
	TCPClient * client = new TCPClient(context,"192.168.1.1", "288", render);



	//Initialize User Buttons
	int mouseButtonLeft = GLFW_RELEASE;
	int mouseButtonRight = GLFW_RELEASE;
	int keyboard_s = GLFW_RELEASE;
	int keyboard_p = GLFW_RELEASE;
	int keyboard_v = GLFW_RELEASE;
	int keyboard_f = GLFW_RELEASE;
	int keyboard_minus = GLFW_RELEASE;
	int keyboard_plus = GLFW_RELEASE;
	//Run until an ESC key is pressed or window is closed.
	while (glfwGetKey(render->getWindow(), GLFW_KEY_ESCAPE) != GLFW_PRESS &&
		glfwWindowShouldClose(render->getWindow()) == 0) {
		
		//Clear the screen buffer
		glClear(GL_COLOR_BUFFER_BIT);
		
		//Check Left Click
		if (mouseButtonLeft != glfwGetMouseButton(render->getWindow(), GLFW_MOUSE_BUTTON_LEFT) && glfwGetMouseButton(render->getWindow(), GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS) {
			//Get Mouse location
			double xpos, ypos;
			glfwGetCursorPos(render->getWindow(), &xpos, &ypos);
			//Scale to 600 by 600 pixel window then multiplied by zoom
			xpos = render->width * (xpos - 600.0/2.0)/(600.0/2.0);
			ypos = render->height * (600.0/2.0-ypos) / (600.0/2.0);
			
			//Get the Current position and heading
			glm::vec3 pos = render->getRobotPosition();
			std::stringstream command;
			float robotHeadingRads = render->getRobotHeading()*3.1415 / 180.0;
			
			//Calculate Distance from center of screen to mouse
			float distance = sqrt(xpos*xpos + ypos * ypos);
			
			//Calcuate Delta Difference of Robot heading and where we want to go
			float delta_degree = (atan2(-xpos, ypos) - render->getRobotHeading()*3.1415 / 180.0)*180.0 / 3.1415;
			if (delta_degree > 180) {
				delta_degree -= 360.0;
			}
			if (delta_degree < -180) {
				delta_degree += 360.0;
			}
			
			//Send out how to turn and move the robot
			command << "m " << delta_degree <<" "<< distance ;
			std::cout<< "m " << delta_degree << " " << distance << std::endl;
			client->send(command.str());
			//render->addObstacles(pos.x + xpos, pos.y + ypos, 0);
		}

		if (mouseButtonRight != glfwGetMouseButton(render->getWindow(), GLFW_MOUSE_BUTTON_RIGHT) && glfwGetMouseButton(render->getWindow(), GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS) {
			
			//Get Mouse location
			double xpos, ypos;
			glfwGetCursorPos(render->getWindow(), &xpos, &ypos);
			//Scale to 600 by 600 pixel window then multiplied by zoom
			xpos = render->width * (xpos - 600.0/2.0)/(600.0/2.0);
			ypos = render->height * (600.0/2.0-ypos) / (600.0/2.0);
			
			//Get the Current position and heading
			glm::vec3 pos = render->getRobotPosition();
			std::stringstream command;
			float robotHeadingRads = render->getRobotHeading()*3.1415 / 180.0;
			
			//Calculate Distance from center of screen to mouse
			float distance = sqrt(xpos*xpos + ypos * ypos);
			
			//Calcuate Delta Difference of Robot heading and where we want to go
			float delta_degree = (atan2(-xpos, ypos) - render->getRobotHeading()*3.1415 / 180.0)*180.0 / 3.1415;
			if (delta_degree > 180) {
				delta_degree -= 360.0;
			}
			if (delta_degree < -180) {
				delta_degree += 360.0;
			}
			
			//Send out how to turn and move the robot
			command << "m " << delta_degree << " " << 0.0;
			//render->setRobotPosition(pos.x + xpos, pos.y + ypos, 0);
			client->send(command.str());
			//render->addObstacles(pos.x + xpos, pos.y + ypos, 0);
		}


		if (keyboard_f != glfwGetKey(render->getWindow(), GLFW_KEY_F) && glfwGetKey(render->getWindow(), GLFW_KEY_F) == GLFW_PRESS) {
			//Get Mouse location
			double xpos, ypos;
			glfwGetCursorPos(render->getWindow(), &xpos, &ypos);
			//Scale to 600 by 600 pixel window then multiplied by zoom
			xpos = render->width * (xpos - 600.0/2.0)/(600.0/2.0);
			ypos = render->height * (600.0/2.0-ypos) / (600.0/2.0);
			
			//Get the Current position and heading
			glm::vec3 pos = render->getRobotPosition();
			std::stringstream command;
			float robotHeadingRads = render->getRobotHeading()*3.1415 / 180.0;
			
			//Calculate Distance from center of screen to mouse
			float distance = sqrt(xpos*xpos + ypos * ypos);
			
			//Calcuate Delta Difference of Robot heading and where we want to go
			float delta_degree = (atan2(-xpos, ypos) - render->getRobotHeading()*3.1415 / 180.0)*180.0 / 3.1415;
			if (delta_degree > 180) {
				delta_degree -= 360.0;
			}
			if (delta_degree < -180) {
				delta_degree += 360.0;
			}
			
			//Send out how to turn and move the robot
			command << "f " << delta_degree << " " << distance;
			std::cout << "f " << delta_degree << " " << distance << std::endl;
			client->send(command.str());
		}

		//Activate Sensor Sweep
		if (keyboard_s != glfwGetKey(render->getWindow(), GLFW_KEY_S) && glfwGetKey(render->getWindow(), GLFW_KEY_S) == GLFW_PRESS) {
			client->send(std::string("s"));
		}



		//Play a sound
		if (keyboard_p != glfwGetKey(render->getWindow(), GLFW_KEY_P) && glfwGetKey(render->getWindow(), GLFW_KEY_P) == GLFW_PRESS) {
			client->send(std::string("p"));
		}
		
		//Place a obstacle on map
		if (keyboard_v != glfwGetKey(render->getWindow(), GLFW_KEY_V) && glfwGetKey(render->getWindow(), GLFW_KEY_V) == GLFW_PRESS) {
			//Get mouse coordinates scaled by window size in pixels.
			double xpos, ypos;
			glfwGetCursorPos(render->getWindow(), &xpos, &ypos);
			xpos = render->width * (xpos - 600.0 / 2.0) / (600.0 / 2.0);
			ypos = render->height * (600.0 / 2.0 - ypos) / (600.0 / 2.0);

			render->addObstacles(xpos, ypos, 4);
		}
		//Zoom in by 500mm
		if (keyboard_minus != glfwGetKey(render->getWindow(), GLFW_KEY_MINUS) && glfwGetKey(render->getWindow(), GLFW_KEY_MINUS) == GLFW_PRESS) {
			render->setView(-500);
		}
		//Zoom out by 500mm
		if (keyboard_plus != glfwGetKey(render->getWindow(), GLFW_KEY_EQUAL) && glfwGetKey(render->getWindow(), GLFW_KEY_EQUAL) == GLFW_PRESS) {
			render->setView(500);
		}

		//Set button to current state so we can only capture one event per press.
		keyboard_p = glfwGetKey(render->getWindow(), GLFW_KEY_P);
		keyboard_s = glfwGetKey(render->getWindow(), GLFW_KEY_S);
		keyboard_v = glfwGetKey(render->getWindow(), GLFW_KEY_V);
		keyboard_f = glfwGetKey(render->getWindow(), GLFW_KEY_F);
		keyboard_minus = glfwGetKey(render->getWindow(), GLFW_KEY_MINUS);
		keyboard_plus = glfwGetKey(render->getWindow(), GLFW_KEY_EQUAL);
		mouseButtonLeft = glfwGetMouseButton(render->getWindow(), GLFW_MOUSE_BUTTON_LEFT);
		mouseButtonRight = glfwGetMouseButton(render->getWindow(), GLFW_MOUSE_BUTTON_RIGHT);
		
		//Render in this order as every render overwrites
		//Thus the priority we want to see is:
		//1) Heading Arrow
		//2) Robot
		//3) Sensor Data
		//4) Obstacles
		//5) Sonar Line
		//6) Map
		render->renderMap();
		render->renderSonarLine();
		render->renderObstacles();
		render->renderSensors();
		render->renderRobot();
		render->renderHeading();
		
		//Render to Screen
		glfwSwapBuffers(render->getWindow());
		//Poll for Events
		glfwPollEvents();

	}
	
	//Data Management and Garbage Collection
	delete client;
	delete render;
return 0;
}