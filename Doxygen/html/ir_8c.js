var ir_8c =
[
    [ "adc_handler", "ir_8c.html#ae93f8650ba123c393dee0fb921eb4dc6", null ],
    [ "adc_init", "ir_8c.html#a1e942fcd91f79d49e4ddb8d7e9d3ef95", null ],
    [ "ADC_read", "ir_8c.html#a5bbea9320798ccfd596c7925f65e7734", null ],
    [ "getADCReading", "ir_8c.html#a7e70a945ffc15434f5310c8684643eb3", null ],
    [ "ir_convertToDist", "ir_8c.html#a9350c2275f27f9622b8253d3f2126cb3", null ],
    [ "DistanceArray", "ir_8c.html#aa52a23ae8bf13149aa43b18d5d41dbbd", null ],
    [ "read", "ir_8c.html#ad908bf08c27e50ab079aa9f459c329af", null ],
    [ "VoltageArray", "ir_8c.html#a9fd5287ec7a5f1b511a3d5512551c8ad", null ]
];