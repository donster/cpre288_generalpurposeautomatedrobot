var searchData=
[
  ['get_5fsonic_5freading',['get_sonic_reading',['../sonic_8c.html#adbe32feee06ccd7a0a9c14709b9a1ce4',1,'get_sonic_reading():&#160;sonic.c'],['../sonic_8h.html#adbe32feee06ccd7a0a9c14709b9a1ce4',1,'get_sonic_reading():&#160;sonic.c']]],
  ['getadcreading',['getADCReading',['../ir_8c.html#a7e70a945ffc15434f5310c8684643eb3',1,'getADCReading():&#160;ir.c'],['../ir_8h.html#a7e70a945ffc15434f5310c8684643eb3',1,'getADCReading():&#160;ir.c']]],
  ['getdegrees',['getDegrees',['../open__interface_8c.html#a22afee5afdd7d60bc1bd9d58a428e4b7',1,'getDegrees(oi_t *self):&#160;open_interface.c'],['../open__interface_8h.html#a22afee5afdd7d60bc1bd9d58a428e4b7',1,'getDegrees(oi_t *self):&#160;open_interface.c']]],
  ['getlastline',['getLastLine',['../uart_8c.html#a46df527ddf86c0ebfe750173a41dd4d3',1,'getLastLine():&#160;uart.c'],['../uart_8h.html#a46df527ddf86c0ebfe750173a41dd4d3',1,'getLastLine():&#160;uart.c']]],
  ['getposition',['getPosition',['../movementcommands_8c.html#a35f84947bb1dd6f437da3949f213c373',1,'getPosition(float *x, float *y, float *d):&#160;movementcommands.c'],['../movementcommands_8h.html#a35f84947bb1dd6f437da3949f213c373',1,'getPosition(float *x, float *y, float *d):&#160;movementcommands.c']]],
  ['getrobotheading',['getRobotHeading',['../class_render.html#ae3f76a2d2e8e781d24a8fb02f5c491fe',1,'Render']]],
  ['getrobotposition',['getRobotPosition',['../class_render.html#a6505f30cb1ff7180fbaac9354ab126b0',1,'Render']]],
  ['getwindow',['getWindow',['../class_render.html#a21c4e9a80a09ee3f649daf1d522b7ebf',1,'Render']]],
  ['go_5fcharge',['go_charge',['../open__interface_8c.html#a6c701c5d6642b0f7128b03542a09ed76',1,'go_charge(void):&#160;open_interface.c'],['../open__interface_8h.html#a6c701c5d6642b0f7128b03542a09ed76',1,'go_charge(void):&#160;open_interface.c']]],
  ['ground',['GROUND',['../cliff__sensor_8h.html#a2fb802f7c8eefcfca976dff0572ea54ba77353523aa43282a05d23c037e67bbda',1,'cliff_sensor.h']]]
];
