var searchData=
[
  ['checkgroundsensors',['checkGroundSensors',['../cliff__sensor_8c.html#aea9fa7041962d3fde3549a56f8855162',1,'checkGroundSensors(oi_t *self):&#160;cliff_sensor.c'],['../cliff__sensor_8h.html#aea9fa7041962d3fde3549a56f8855162',1,'checkGroundSensors(oi_t *self):&#160;cliff_sensor.c']]],
  ['chirp_5finit',['chirp_init',['../sonic_8c.html#a026052969f1c177a4c0c035b1ea5b9e7',1,'chirp_init():&#160;sonic.c'],['../sonic_8h.html#a026052969f1c177a4c0c035b1ea5b9e7',1,'chirp_init():&#160;sonic.c']]],
  ['cliff',['CLIFF',['../cliff__sensor_8h.html#a2fb802f7c8eefcfca976dff0572ea54bac73ff93c632d693d7e8eb610091ce637',1,'cliff_sensor.h']]],
  ['cliff_5fsensor_2ec',['cliff_sensor.c',['../cliff__sensor_8c.html',1,'']]],
  ['cliff_5fsensor_2eh',['cliff_sensor.h',['../cliff__sensor_8h.html',1,'']]],
  ['cliffsensor',['CliffSensor',['../cliff__sensor_8h.html#a063460cc4181aaf54ece2e9b6b1d5ac4',1,'cliff_sensor.h']]],
  ['clock_5fdiv',['CLOCK_DIV',['../uart_8c.html#a51c63a5aa652b57ab011f9424d97f32a',1,'uart.c']]],
  ['clock_5ftimer_5finit',['clock_timer_init',['../timer4_8c.html#afe6c9dd8b2439c58273cbd63b95088fe',1,'clock_timer_init(void):&#160;timer4.c'],['../timer4_8h.html#afe6c9dd8b2439c58273cbd63b95088fe',1,'clock_timer_init(void):&#160;timer4.c']]],
  ['cs_5fgetterrain',['cs_getTerrain',['../cliff__sensor_8c.html#a1fa03a7f9a05ace91224ea610463e871',1,'cs_getTerrain(oi_t *self, CliffSensor sensor):&#160;cliff_sensor.c'],['../cliff__sensor_8h.html#a1fa03a7f9a05ace91224ea610463e871',1,'cs_getTerrain(oi_t *self, CliffSensor sensor):&#160;cliff_sensor.c']]],
  ['cs_5freadsensorraw',['cs_readSensorRaw',['../cliff__sensor_8c.html#ad8ad20a05cb459df743b6e8a4ed37be6',1,'cs_readSensorRaw(oi_t *self, CliffSensor sensor):&#160;cliff_sensor.c'],['../cliff__sensor_8h.html#ad8ad20a05cb459df743b6e8a4ed37be6',1,'cs_readSensorRaw(oi_t *self, CliffSensor sensor):&#160;cliff_sensor.c']]]
];
