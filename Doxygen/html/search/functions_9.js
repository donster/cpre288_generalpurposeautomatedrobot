var searchData=
[
  ['send',['send',['../class_t_c_p_client.html#af83a77ee901507599e03e55976a99b7d',1,'TCPClient']]],
  ['setrobotposition',['setRobotPosition',['../class_render.html#ae295d0c33bdce379be5e30f45455170b',1,'Render']]],
  ['setsonarline',['setSonarLine',['../class_render.html#af8df9aaab30d3a4a981f5b230a087a8f',1,'Render']]],
  ['setview',['setView',['../class_render.html#a8f9e3fbd4a2142fea7d5933c54c36ee1',1,'Render']]],
  ['sonic_5fchirp',['sonic_chirp',['../sonic_8c.html#a588b501da316bcf0dec4756000d5ebce',1,'sonic_chirp():&#160;sonic.c'],['../sonic_8h.html#a588b501da316bcf0dec4756000d5ebce',1,'sonic_chirp():&#160;sonic.c']]],
  ['sonic_5fconverttodist',['sonic_convertToDist',['../sonic_8c.html#ab57b797a1fd5d1fbfcdbc150282bff91',1,'sonic_convertToDist(int rawSonicValue):&#160;sonic.c'],['../sonic_8h.html#ab57b797a1fd5d1fbfcdbc150282bff91',1,'sonic_convertToDist(int rawSonicValue):&#160;sonic.c']]],
  ['sonic_5fhandler',['sonic_handler',['../sonic_8c.html#a6ddb01fc5fc0c1b2931dbd1705c5b6b1',1,'sonic_handler():&#160;sonic.c'],['../sonic_8h.html#a6ddb01fc5fc0c1b2931dbd1705c5b6b1',1,'sonic_handler():&#160;sonic.c']]],
  ['sonic_5finit',['sonic_init',['../sonic_8c.html#a0cd05251349a248cfafffcea2499cd15',1,'sonic_init():&#160;sonic.c'],['../sonic_8h.html#a0cd05251349a248cfafffcea2499cd15',1,'sonic_init():&#160;sonic.c']]],
  ['sonic_5fstart',['sonic_start',['../sonic_8c.html#a0176373c7a145890460b8b194e3ca0ce',1,'sonic_start():&#160;sonic.c'],['../sonic_8h.html#a0176373c7a145890460b8b194e3ca0ce',1,'sonic_start():&#160;sonic.c']]],
  ['start',['start',['../class_t_c_p_client.html#a60de64d75454385b23995437f1d72669',1,'TCPClient']]],
  ['sweep_5fdofullsweep',['sweep_doFullSweep',['../sweep__sensor_8c.html#a967045ec4778e4ef6fc5b6d9aba3a80d',1,'sweep_doFullSweep(float sweepIncrement_deg, uint32_t incDelay_ms):&#160;sweep_sensor.c'],['../sweep__sensor_8h.html#a967045ec4778e4ef6fc5b6d9aba3a80d',1,'sweep_doFullSweep(float sweepIncrement_deg, uint32_t incDelay_ms):&#160;sweep_sensor.c']]],
  ['sweep_5fdosweep',['sweep_doSweep',['../sweep__sensor_8c.html#ad299d740e7020063a809deb1e7401d92',1,'sweep_doSweep(float startAngle_deg, float endAngle_deg, float sweepIncrement_deg, uint32_t incDelay_ms):&#160;sweep_sensor.c'],['../sweep__sensor_8h.html#ad299d740e7020063a809deb1e7401d92',1,'sweep_doSweep(float startAngle_deg, float endAngle_deg, float sweepIncrement_deg, uint32_t incDelay_ms):&#160;sweep_sensor.c']]],
  ['sweep_5fgetpos',['sweep_getPos',['../sweep__sensor_8c.html#a111f017ba6449291db5ba445d602659c',1,'sweep_getPos():&#160;sweep_sensor.c'],['../sweep__sensor_8h.html#a111f017ba6449291db5ba445d602659c',1,'sweep_getPos():&#160;sweep_sensor.c']]],
  ['sweep_5finit',['sweep_init',['../sweep__sensor_8c.html#aa858319b04e153550a93648468db3171',1,'sweep_init():&#160;sweep_sensor.c'],['../sweep__sensor_8h.html#aa858319b04e153550a93648468db3171',1,'sweep_init():&#160;sweep_sensor.c']]],
  ['sweep_5fmoveabsolute',['sweep_moveAbsolute',['../sweep__sensor_8c.html#a87706abc24e10327499a2612a0ae3cbc',1,'sweep_moveAbsolute(float angularPos_deg):&#160;sweep_sensor.c'],['../sweep__sensor_8h.html#a87706abc24e10327499a2612a0ae3cbc',1,'sweep_moveAbsolute(float angularPos_deg):&#160;sweep_sensor.c']]],
  ['sweep_5fmoverelative',['sweep_moveRelative',['../sweep__sensor_8c.html#a3570da170a331a34c81a23922b3197fc',1,'sweep_moveRelative(float angularOffset_deg):&#160;sweep_sensor.c'],['../sweep__sensor_8h.html#a3570da170a331a34c81a23922b3197fc',1,'sweep_moveRelative(float angularOffset_deg):&#160;sweep_sensor.c']]],
  ['sweep_5freadirsensor',['sweep_readIRSensor',['../sweep__sensor_8c.html#af1fcacf7ec9956ea6fb31a62f593d6e7',1,'sweep_readIRSensor():&#160;sweep_sensor.c'],['../sweep__sensor_8h.html#af1fcacf7ec9956ea6fb31a62f593d6e7',1,'sweep_readIRSensor():&#160;sweep_sensor.c']]],
  ['sweep_5freadsonicsensor',['sweep_readSonicSensor',['../sweep__sensor_8c.html#a059b6d41eb8934ac26e0928818843c6e',1,'sweep_readSonicSensor():&#160;sweep_sensor.c'],['../sweep__sensor_8h.html#a059b6d41eb8934ac26e0928818843c6e',1,'sweep_readSonicSensor():&#160;sweep_sensor.c']]]
];
