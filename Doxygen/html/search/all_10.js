var searchData=
[
  ['tcp_2ecpp',['tcp.cpp',['../tcp_8cpp.html',1,'']]],
  ['tcp_2eh',['tcp.h',['../tcp_8h.html',1,'']]],
  ['tcp_5fcommand_5ftype',['TCP_COMMAND_TYPE',['../movementcommands_8h.html#a0f186fb4d8b38b2716599e2b5cc65945',1,'movementcommands.h']]],
  ['tcpclient',['TCPClient',['../class_t_c_p_client.html',1,'TCPClient'],['../class_t_c_p_client.html#acc9b868574c5cf4325f931c2a3ec9249',1,'TCPClient::TCPClient()']]],
  ['terrain',['Terrain',['../cliff__sensor_8h.html#a2fb802f7c8eefcfca976dff0572ea54b',1,'cliff_sensor.h']]],
  ['timer_2ec',['timer.c',['../timer_8c.html',1,'']]],
  ['timer_2eh',['timer.h',['../timer_8h.html',1,'']]],
  ['timer4_2ec',['timer4.c',['../timer4_8c.html',1,'']]],
  ['timer4_2eh',['timer4.h',['../timer4_8h.html',1,'']]],
  ['timer4_5fprescaler',['TIMER4_PRESCALER',['../timer4_8c.html#a97bf08dc423d9cdc2da93639cb844024',1,'timer4.c']]],
  ['timer4a_5fcounts',['TIMER4A_COUNTS',['../timer4_8c.html#a331ba268ffcb9f2d37028b9131337cf7',1,'timer4.c']]],
  ['timer4b_5fcounts',['TIMER4B_COUNTS',['../timer4_8c.html#a4f99040cc6b5959c0e0cca1af97df207',1,'timer4.c']]],
  ['timer_5fticks_5fper_5fmicros',['TIMER_TICKS_PER_MICROS',['../sweep__sensor_8c.html#a6579d7b211ae42d014bae9f1e9244b86',1,'sweep_sensor.c']]],
  ['timer_5fticks_5fper_5fsecond',['TIMER_TICKS_PER_SECOND',['../sweep__sensor_8c.html#a81c686546c2c34dce41931315aa910df',1,'sweep_sensor.c']]],
  ['timer_5fwaitmillis',['timer_waitMillis',['../timer_8c.html#a7e2a3b4520e885e5e7d40fa87e242a75',1,'timer_waitMillis(uint32_t millis):&#160;timer.c'],['../timer_8h.html#a7e2a3b4520e885e5e7d40fa87e242a75',1,'timer_waitMillis(uint32_t millis):&#160;timer.c']]],
  ['turnrobotdegree',['turnRobotDegree',['../movementcommands_8c.html#a363d2d2063890c9731f0d9222060cd48',1,'turnRobotDegree(oi_t *self, float degrees):&#160;movementcommands.c'],['../movementcommands_8h.html#a363d2d2063890c9731f0d9222060cd48',1,'turnRobotDegree(oi_t *self, float degrees):&#160;movementcommands.c']]]
];
