var indexSectionsWithContent =
{
  0: "abcdefghilmopqrstuvwz~",
  1: "ort",
  2: "cilmorstu",
  3: "acgilmoprstu~",
  4: "bdehlmprsvw",
  5: "ct",
  6: "bcefglmpqrsz",
  7: "bcmprstv"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Macros"
};

