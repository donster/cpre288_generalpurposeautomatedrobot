var searchData=
[
  ['checkgroundsensors',['checkGroundSensors',['../cliff__sensor_8c.html#aea9fa7041962d3fde3549a56f8855162',1,'checkGroundSensors(oi_t *self):&#160;cliff_sensor.c'],['../cliff__sensor_8h.html#aea9fa7041962d3fde3549a56f8855162',1,'checkGroundSensors(oi_t *self):&#160;cliff_sensor.c']]],
  ['chirp_5finit',['chirp_init',['../sonic_8c.html#a026052969f1c177a4c0c035b1ea5b9e7',1,'chirp_init():&#160;sonic.c'],['../sonic_8h.html#a026052969f1c177a4c0c035b1ea5b9e7',1,'chirp_init():&#160;sonic.c']]],
  ['clock_5ftimer_5finit',['clock_timer_init',['../timer4_8c.html#afe6c9dd8b2439c58273cbd63b95088fe',1,'clock_timer_init(void):&#160;timer4.c'],['../timer4_8h.html#afe6c9dd8b2439c58273cbd63b95088fe',1,'clock_timer_init(void):&#160;timer4.c']]],
  ['cs_5fgetterrain',['cs_getTerrain',['../cliff__sensor_8c.html#a1fa03a7f9a05ace91224ea610463e871',1,'cs_getTerrain(oi_t *self, CliffSensor sensor):&#160;cliff_sensor.c'],['../cliff__sensor_8h.html#a1fa03a7f9a05ace91224ea610463e871',1,'cs_getTerrain(oi_t *self, CliffSensor sensor):&#160;cliff_sensor.c']]],
  ['cs_5freadsensorraw',['cs_readSensorRaw',['../cliff__sensor_8c.html#ad8ad20a05cb459df743b6e8a4ed37be6',1,'cs_readSensorRaw(oi_t *self, CliffSensor sensor):&#160;cliff_sensor.c'],['../cliff__sensor_8h.html#ad8ad20a05cb459df743b6e8a4ed37be6',1,'cs_readSensorRaw(oi_t *self, CliffSensor sensor):&#160;cliff_sensor.c']]]
];
