var movementcommands_8h =
[
    [ "TCP_COMMAND_TYPE", "movementcommands_8h.html#a0f186fb4d8b38b2716599e2b5cc65945", [
      [ "ERROR", "movementcommands_8h.html#a0f186fb4d8b38b2716599e2b5cc65945a2fd6f336d08340583bd620a7f5694c90", null ],
      [ "MOVE", "movementcommands_8h.html#a0f186fb4d8b38b2716599e2b5cc65945aed3ef32890b6da0919b57254c5206c62", null ],
      [ "PLAY_SOUND", "movementcommands_8h.html#a0f186fb4d8b38b2716599e2b5cc65945a033ccbe4af91f1a1162f4bf7623d6f44", null ],
      [ "SWEEP_SENSOR", "movementcommands_8h.html#a0f186fb4d8b38b2716599e2b5cc65945a12e905ca3c2109051b98bdbbb78992af", null ],
      [ "QUERY_MOVEMENT", "movementcommands_8h.html#a0f186fb4d8b38b2716599e2b5cc65945a6c69581a1d4f4abf35afd72f8e44317f", null ],
      [ "MOVE_IGNORE", "movementcommands_8h.html#a0f186fb4d8b38b2716599e2b5cc65945a9b233283397dc4f25de3e8ac2a1bc4a2", null ]
    ] ],
    [ "getPosition", "movementcommands_8h.html#a35f84947bb1dd6f437da3949f213c373", null ],
    [ "moveRobotDistance", "movementcommands_8h.html#ac19e613d92cdd54f047e591fc0c99b07", null ],
    [ "moveRobotDistanceIgnore", "movementcommands_8h.html#aec736dae690e0a896c49d05b27c9b970", null ],
    [ "parseCommand", "movementcommands_8h.html#af24f4aea581508f13025bf49cdb40b44", null ],
    [ "turnRobotDegree", "movementcommands_8h.html#a363d2d2063890c9731f0d9222060cd48", null ]
];