var cliff__sensor_8h =
[
    [ "CliffSensor", "cliff__sensor_8h.html#a063460cc4181aaf54ece2e9b6b1d5ac4", [
      [ "LEFT", "cliff__sensor_8h.html#a063460cc4181aaf54ece2e9b6b1d5ac4adb45120aafd37a973140edee24708065", null ],
      [ "FRONT_LEFT", "cliff__sensor_8h.html#a063460cc4181aaf54ece2e9b6b1d5ac4a3e4b80cb31443019e807a14549b1d2bd", null ],
      [ "FRONT_RIGHT", "cliff__sensor_8h.html#a063460cc4181aaf54ece2e9b6b1d5ac4aae8a476ec2a06c02ffb4074660093318", null ],
      [ "RIGHT", "cliff__sensor_8h.html#a063460cc4181aaf54ece2e9b6b1d5ac4aec8379af7490bb9eaaf579cf17876f38", null ]
    ] ],
    [ "Terrain", "cliff__sensor_8h.html#a2fb802f7c8eefcfca976dff0572ea54b", [
      [ "CLIFF", "cliff__sensor_8h.html#a2fb802f7c8eefcfca976dff0572ea54bac73ff93c632d693d7e8eb610091ce637", null ],
      [ "GROUND", "cliff__sensor_8h.html#a2fb802f7c8eefcfca976dff0572ea54ba77353523aa43282a05d23c037e67bbda", null ],
      [ "ZONE", "cliff__sensor_8h.html#a2fb802f7c8eefcfca976dff0572ea54baa7eaefe48c50e03076ec26780d1b8248", null ],
      [ "BOUNDARY", "cliff__sensor_8h.html#a2fb802f7c8eefcfca976dff0572ea54babb01c29d92244006bbc24ac5229da199", null ]
    ] ],
    [ "checkGroundSensors", "cliff__sensor_8h.html#aea9fa7041962d3fde3549a56f8855162", null ],
    [ "cs_getTerrain", "cliff__sensor_8h.html#a1fa03a7f9a05ace91224ea610463e871", null ],
    [ "cs_readSensorRaw", "cliff__sensor_8h.html#ad8ad20a05cb459df743b6e8a4ed37be6", null ]
];